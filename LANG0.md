# lang0 specifications

> lang0 stands for stupid language

[[_TOC_]]

## Structure of a lang0 program

A lang0 program is defined as a (possibly empty) sequence of language items.
Language items include:
- type definitions
- function definitions 
- constant definitions
- persistents definitions

## Built-in types

lang0 provides several kinds of built-in types.

### Basic types and literals

The provided numeric types : 

| type identifier         | description             | literal         |
|:-----------------------:|:------------------------|:---------------:|
|`char`                   | characters              |`'c'`            |
|`u8`, `u16`, `u32`, `u64`| unsigned integers       | `100`           |
|`i8`, `i16`, `i32`, `i64`| signed integers         | `101`           |
|`f32`, `f64`             | floating-point numbers  | `100.01`        |
|`bool`                   | boolean values          | `true`, `false` |

### Pointers and pointer literals

| type identifier | literal            |
|:---------------:|:------------------:|
| `&usize`        | `&x` if `x: usize` |

TODO

### Arrays and array literal

Arrays are built-in and are _not just_ syntactic sugar for pointers.

| type identifier | literal           |
|:---------------:|:-----------------:|
| `[char, 3]`     | `['a', 'b', 'c']` |

Array indexing zero-based and is done through the classic `array[idx]` bracket
notation. If an array is instanciated with an array literal of size 1, for
example `[0]`, then the entire array is filled with the expression in brackets.

### Tuples and tuple literals

Unnamed tuples allow agregation of heteregeous data. They are fixed sized, and
their fields are public.

| type identifier | literal     |
|:---------------:|:-----------:|
| `(char, usize)` | `('b', 10)` |

Field access is done through the `tuple[idx]` notation and is also zero-based.
Note that the index has to be a compile-time constant (i.e. you can't iterate
through a tuple in loop).

## Variables

lang0 provides different ways to name values through the use of constants,
persistents and locals.

### Constants

Constants are scoped immutable values known at compile-time. 

```lang0
let const SIZE : usize = 10;
let const D : char = 'd';
```

Constants must be typed when created. They are inlined when refered to with
their identifier.

### Persistents

Persistents (or statics) are scoped values that live until the end the program.

```lang0
let persistent COUNT : usize = 0;
```

Persistents must be typed when created. They are initialized once and their
values persist across function calls (if defined within a function). All
subsequent initialization statements are ignored. Persistent variables are
always mutable.

### Locals

Locals are scoped values that live until the end of their code block. 

```lang0
let x : usize = 10;
let mut y : usize = 10;
let z := 10.0;
```

Locals are immutable by default but can be made mutable with the `mut` keyword.
Their type is optional, and will be infered from context when absent.

### Assignments

Persistent and local variables can be re-assigned a value after their
initialization with the assignment operation.

```lang0
let mut x : usize = 10;
x = 20;

let persistent COUNT : usize = 0;
COUNT = COUNT + 1;
```

When re-assigned, the previous value is discarded. More generally, 
[lvalues](not_implemented) can be reassigned.

## Functions 

### Function definitions

Functions are defined using the `fn` keyword. Function parameters must be typed,
and the return type must be specified only when the function returns something.
The function body is enclosed within a `begin`-`end` block. The function exits
once it reaches the `return` keyword or if it reaches the end its body. To exit
early in a non-returning function, use `return ()`.

```lang0
fn diff(a: f64, b: f64): f64 begin
	return b - a;
end

fn zero(): f64 begin
	return 0.0;
end
```

A function body is composed of language items, assignments and expressions.
Therefore a function can contain local type definitions as well as local
functions.

### Function calls & lvalues

Once a function is defined it can be called using the `()` function-call
operator, with the appropriate arguments corresponding to the function. The
function-call operator as well as the `[]` indexing operator, the `.` member
access operator,  the `this` reference operator and the `::` scope resolution
operator allow us to define lvalues as any combination of these operators,
acting on identifiers. Our particular definition results in these operators
being left associative, i.e. `this.array[].function()` is equivalent to
`(((this.array)[]).function)()`.

## User-defined types

The language allows the use to define several kind of types, namely records,
enums and named tuples. A type is defined by the `type` operator, followed by
an identifier (the name for the new type) and then a "type constructor".

Some examples of type definitions are:

```lang0
type Real = f64;

type Point = (Real, Real);

type Direction = enum
	Direct,
	Indirect,
end

type Line = record 
	public a: Point,
	public b: Point,
	direction: Direction,
end

type Polygon = enum 
	Triangle = (Point, Point, Point),
	Decagon = ([Point, 10]),
	Circle = (Point, Real),
end
```


### Type aliases and type distinction

When the right-hand side of the type definition is a numeric type, a type
identifier or an array, the type definition is considered to be a type alias,
which means the type identifier on the left-hand side and the type on the
right-hand side are interchangeable. A consequence of this is that any methods
implemented on the type alias are just methods implemented for the original
type.

When the right-hand side of the type definition is a tuple, a record or an enum,
the type definition creates a whole new type. A consequence is that even though
two types can be defined with exactly the same specifications, they will
considered to be distinct and __not__ interchangeable. For example in

```lang0
type RealPair = (f64, f64);
type ReallyRealPair = (f64, f64);
```

the types `RealPair` and `ReallyRealPair` are not interchangeable in any way. 

Another example is the two following type definition.

```lang0
type Duration1 = f64;
type Duration2 = (f64);
type Length = (f64);
```

The first definition creates a type alias for `f64`, the second and third create
tuples with only one field, containing an `f64`. This distinction is important
when you want to give different meanings for things with the same underlying
representation but different use-cases, and you want to prevent its mishandling.

### Named tuples

Named tuples act exactly like an unnamed tuple with the exception that their
fields are [private](visibility-of-type-fields) and only accessible in methods,
and that, because of the type distinction rule, they will be considered
different from an homomorphic unnamed or named tuple, allowing for
contextualization of data without naming the fields like a struct.

```lang0
type Vector2D = (f64, f64);
```

Named tuples literals are produced by preceding an unnamed tuple literal with the
type identifier of the wanted named tuple.

```lang0
let v := Vector2D(3.0, 0.0);
```

### Records

A record type is an agregation of a finite number of types, with the addition
that each field is named. 

```lang0
type ComplexF64 = record
	re: f64,
	im: f64
end
```

Fields are accessed through the `.` member access operator.

Records are instantiated using a raw record literal, which is a block beginning
by the `record` keyword and the type identifier of the record, followed by a
list a named arguments and closed with the `end` keyword.

```
let z := record ComplexF64 
	re = 3.0, 
	im = 0.0 
end;
```

### Enums

An enum type consists of a finite number of variants, and each variant can be
associated to an unnamed tuple type.

```lang0
type MaybeUsize = enum
	Just = (usize),
	None,
end
```

Enums literals are produced by naming the variant using the [scope resolution
operator](not_implemented), and following it with its associated tuple (if the
variant has one).

```
let x := MaybeUsize::None;
let y := MaybeUsize::Just(2);
```

### Visibility of type fields

When a type has fields, the fields have a visibility:
 - a public visibility means that the field is accessible everywhere outside of
   the type
 - a private visibility means that the field is accsesible only within the type
   methods

Record fields are private by default but can be independently set public with
the `public` keyword. Unnamed tuple fields and fields of a tuple-variant of an
enum are public, and the visibility cannot be changed. Named tuple fields are
private, and the visibility cannot be changed (note that fields can still be
exposed by defining a method on the type).

## Type methods

The language allows user-defined types to have member functions (aka methods),
defined in an `impl` block. Methods are written the exact same way as functions,
with the addition that they can optionally take `this` as a function parameter,
refering to the current instance of the type. When the `this` keyword is absent
from a method signature, the method is treated as static and type fields are
inacessible.

```lang0
type UsizeArrayVec = record
	it: [usize, 32],
	len: usize,
end

impl UsizeArrayVec 
	fn new(): UsizeArrayVec begin
		return record UsizeArrayVec it = [0], len = 0 end;
	end

	fn push(this, x: usize) begin
		assert(this.len < 32);
		this.it[this.len] = x;
		this.len = this.len + 1;
	end

	fn pop(this): usize begin
		assert(this.len > 0);
		this.len = this.len - 1;
		return this.it[this.len + 1];
	end
end
```

Static methods are called using the `::` scope resolution operator, and member
methods are called using the `.` member access operator on an instance of the
type and omitting the `this` parameter as it is implicitely passed to the
function.

```lang0
let a := UsizeArrayVec::new();
a.push(3);
print(a.pop());
```

## Expressions

lang0 follows the philosophy of "almost everything is an expression". That is,
usual control flow structures like loops and conditionnals are assigned a value,
as well as normal code blocks.

### Basic expressions

The most fundamental expressions are type literals and lvalues, i.e. number
literals, string literals, boolean literals, record literals, tuple literals,
array literals, enum literals, function & method calls.

### Arithmetic & logical expressions

Basic expressions can be combined with the usual arithmetic operators (`*`, `/`,
`+`, `-`), comparison operators (`==`, `!=`, `<`, `<=`, `>`, `>=`), and logical
operators (`and`, `or`). The precedence is given by order of enumeration in the
previous sentence (i.e. `*` & `/` bind the tightest and `and` and `or` bind the
loosest). All of the operators are left-associative, except for comparison
operators which are not associative (thus cannot be chained). The type of such
an expression is the returning type of the last operator evaluated. Expressions
can also be grouped within parentheses, following the usual rule that
expressions within them will be evaluated first.

```lang0
 let x := 5*(2 + 3) - 25;
 let y := x == 0 or 5 > 2;
 assert(y);
```

### Control Flow

lang0 provides different control flow structures:
- code blocks with `begin ... end`
- infinite loops with `loop ... end` 
- iterator loops with `for ... in ... do`
- conditionals with `if ... then ... elif ... then ... else ... end`

#### Code blocks

Code blocks are a sequence of statements bounded by the `begin` and `end`
keywords. A code block may end with an expression, which will be the value of
the code block expression.

```lang0
begin
	let x := 10;
	let y := 2 * x;
end

let z := begin
	let x := 10;
	2 * x
end;
```

#### Conditionals

##### `if` expressions 

lang0 provides the usual if-then-else statements, allowing for conditional
execution of code blocks. Additional branches may be specified with the `elif`
keyword. All branches (i.e `elif` and `else`) are optional except when yielding
an expression, in that case the `else` branch is mandatory.

```lang0
let x := 2;
if x == 2 then
	print("x == 2");
elif x == 3 then 
	print("x == 3");
else
	print("idk :'(");
end

let y := if x > 0 then 1 else -1 end;
```

The value of an `if` expression will be the value of the last expression in each
branches (meaning each branch must yield the same type).

#### Loops

##### `loop` expressions

The `loop` keyword allows to define a code blocks that will execute infinitely,
or until it reaches a `break` expression, which will be the value yielded by the
loop.

```lang0
let z := 0;
loop
	z = z + 1;
	
	if z == 10 then
		break;
	end
end;

let w := loop
	z = z + 1;

	if z == 15 then
		break 32;
	end
end;
```

If a `loop` block contains multiple `break` statements, they must all have the
same type.

##### `for` expressions

The `for` keyword allows to define an iterator (i.e. arrays for now) loop. The
value associated to the for expression will either be the last expression in the
loop body at the last iteration, or the value yielded by a `break` expression.

```lang0
let n := 0;
for i in [1, 2, 3] do
	n = 2 * i;
end

n = for i in [1, 2, 3] do
	2 * i
end;
```

If a `for` loop doesn't run (because of an empty iterator), then TODO (I haven't
decided yet).
