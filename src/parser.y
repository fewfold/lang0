%code requires {

	#include "prelude.h"
	#include "diagnostics.h"
	#include "ast.h"

	typedef void* yyscan_t;	

	#undef YYLLOC_DEFAULT
	#define YYLLOC_DEFAULT(cur, rhs, n)                              \
	do {                                                             \
		if (n) {                                                     \
			(cur).start = YYRHSLOC(rhs,1).start;                     \
			(cur).len = YYRHSLOC(rhs,n).start + YYRHSLOC(rhs,n).len; \
		} else {                                                     \
			(cur).start = YYRHSLOC(rhs,0).start;                     \
			(cur).len = 0;                                           \
		}                                                            \
		} while (0) 

	#define ALLOC(expr, type)                          \
		({                                             \
			type* ptr = (type*) malloc(sizeof(type));  \
			*ptr=(expr);                               \
			ptr;                                       \
		})

}

%verbose
%define parse.trace
%define locations
%define api.location.type {Span}
%define parse.error custom
%define api.pure full

%parse-param { CompilerContext* comp_ctx } { yyscan_t scanner } { Program* result }
%lex-param   { yyscan_t scanner }

%define api.value.type union

%token 
	<struct Token> Kind_Ident  "{ident}"
	<struct Literal> Kind_Number "{number}"
	<struct Literal> Kind_String "{string}"
	<struct Literal> Kind_True   "true"
    <struct Literal> Kind_False  "false"
	<struct Literal> Kind_This   "this"

%token
	<struct Type> Kind_Char   "char"
	<struct Type> Kind_F32    "f32"
	<struct Type> Kind_F64    "f64"
	<struct Type> Kind_U8     "u8"
	<struct Type> Kind_U16    "u16"
	<struct Type> Kind_U32    "u32"
	<struct Type> Kind_U64    "u64"
	<struct Type> Kind_Usize  "usize"
	<struct Type> Kind_I8     "i8"
	<struct Type> Kind_I16    "i16"
	<struct Type> Kind_I32    "i32"
	<struct Type> Kind_I64    "i64"
	<struct Type> Kind_Isize  "isize"
	<struct Type> Kind_Bool   "bool"

%type
	<struct Assignment> Assignment
	<struct Block*> IfAlts
	<struct Block*> IfEnd
	<struct Block> Block
	<struct Block> BlockEnd
	<struct EnumVariant> EnumVariant
	<struct EnumVariantSeq> EnumVariantSeq
	<struct Expr> BreakExpr
	<struct Expr> Expr
	<struct Expr> ExprWithBlock
	<struct Expr> ExprWithoutBlock
	<struct Expr> PrimitiveExpr
	<struct Expr> ReturnExpr
	<struct ExprSeq> ExprSeq
	<struct For> ForExpr
	<struct If> IfCond
	<struct If> IfExpr
	<struct Item> Item
	<struct ItemSeq> ItemSeq
	<struct LValue> LValue
	<struct Literal> Literal
	<struct Literal> RecordLiteral
	<struct Loop> LoopExpr
	<struct Stmt> ExprStmt
	<struct Stmt> Stmt
	<struct StmtSeq> StmtSeq

%type
	<struct FunctionDef> FunctionDef
	<struct FunctionDef> FunctionSig
	<struct FunctionDef> MethodDef
	<struct FunctionDef> MethodSig
	<struct FunctionDefSeq> MethodSeq

%type
	<struct Type> TypeIdent
	<struct Type> Array
	<struct Type> BuiltInType
	<struct Type> DefinableType
	<struct Type> Enum
	<struct Type> FunctionRetType
	<struct Type> NameableType
	<struct Type> Record
	<struct Type> Tuple
	<struct Type> TypeAscription
	<struct TypeDef> TypeDef
	<struct TypeImpl> TypeImpl
	<struct TypeSeq> NameableTypeSeq
	<struct TypedParam> RecordField
	<struct TypedParam> TypedIdent
	<struct TypedParamSeq> FunctionParamSeq
	<struct TypedParamSeq> MethodParamSeq
	<struct TypedParamSeq> RecordFieldSeq

%type
	<struct VarDef> VariableDef
	<struct VarDefKind> VarKind

%type
	<usize> NamedArg
	<usize> NamedArgSeq
	<usize> Vis

%token 
	<struct Token> Kind_Type   "type"
	<struct Token> Kind_Eq     "="
	<struct Token> Kind_Public "public"
	<struct Token> Kind_Record "record"
	<struct Token> Kind_Enum   "enum"
	<struct Token> Kind_Fn     "fn"
	<struct Token> Kind_Begin  "begin"
	<struct Token> Kind_End    "end"
	<struct Token> Kind_Impl   "impl"
	<struct Token> Kind_Return "return"


%token 
	<struct Token> Kind_Loop  "loop"
	<struct Token> Kind_For   "for"
	<struct Token> Kind_In    "in"
	<struct Token> Kind_Do    "do"
	<struct Token> Kind_Break "break"
	<struct Token> Kind_If    "if"
	<struct Token> Kind_Then  "then"
	<struct Token> Kind_Else  "else"
	<struct Token> Kind_Elif  "elif"

%token 
	<struct Token> Kind_Let   "let"
	<struct Token> Kind_Mut   "mut"
	<struct Token> Kind_Const "const"
	<struct Token> Kind_Persistent "persistent"

%token 
	<struct Token> Kind_And   "and"
	<struct Token> Kind_Or    "or"
	<struct Token> Kind_Plus  "+"
	<struct Token> Kind_Minus "-"
	<struct Token> Kind_Star  "*"
	<struct Token> Kind_Slash "/"
	<struct Token> Kind_Amp   "&"

%token 
	<struct Token> Kind_EqEq   "=="
	<struct Token> Kind_BangEq "!="
	<struct Token> Kind_LtEq   "<="
	<struct Token> Kind_GtEq   ">="
	<struct Token> Kind_Lt     "<"
	<struct Token> Kind_Gt     ">"

%token 
	<struct Token> Kind_LBracket   "["
	<struct Token> Kind_RBracket   "]"
	<struct Token> Kind_LParen     "("
	<struct Token> Kind_RParen     ")"
	<struct Token> Kind_Comma      ","
	<struct Token> Kind_Colon      ":"
	<struct Token> Kind_Dot        "."
	<struct Token> Kind_Semi       ";"
	<struct Token> Kind_ColonColon "::"

%right "return" "break" "&"
%left "or" "and"
%nonassoc "==" "!=" "<=" ">=" "<" ">"
%left "+" "-"
%left "*" "/"

%start Program

%%

NameableType
	: TypeIdent   { $$ = $1; }
	| BuiltInType { $$ = $1; }
	;

TypeIdent
	: "{ident}" { $$ = namedty_new($1.repr, $1.span); }
	;

BuiltInType
	: "char"  { $$ = $1; }
	| "f32"	  { $$ = $1; }
	| "f64"   { $$ = $1; }
	| "u8"	  { $$ = $1; }
	| "u16"	  { $$ = $1; }
	| "u32"	  { $$ = $1; }
	| "u64"	  { $$ = $1; }
	| "usize" { $$ = $1; }
	| "i8"	  { $$ = $1; }
	| "i16"	  { $$ = $1; }
	| "i32"	  { $$ = $1; }
	| "i64"	  { $$ = $1; }
	| "isize" { $$ = $1; }
	| "bool"  { $$ = $1; }
	| Array   { $$ = $1; }
	| Tuple   { $$ = $1; }
	;

Array
	: "[" NameableType "," "{number}" "]" 
		{ 
			$$ = arrayty_new(
				ALLOC($2, Type), 
				ALLOC(litexpr_new($4, $4.span), Expr), 
				span_merge($1.span, $5.span)); 
		}
	;

Tuple
	: "(" NameableTypeSeq ")" 
		{ 
			$$ = tuplety_new($2, span_merge($1.span, $3.span));
		}
	;

NameableTypeSeq
	: %empty { $$ = typeseq_new(NULL, span_new(0,0)); }
	| NameableType
		{ 
			$$ = typeseq_new(
					ALLOC(typenode_new(ALLOC($1, Type)),TypeNode), 
					$1.span); 
		}
	| NameableType "," NameableTypeSeq 
		{
			typeseq_push(&$3, $1);
			$3.span = span_merge($1.span, $3.span);
			$$ = $3;
		}
	;

VariableDef
	: "let" VarKind "{ident}" ":" TypeAscription "=" Expr ";"
		{
			$$ = vardef_new($2, ALLOC($7, Expr), span_merge($1.span, $8.span));
		}
	;

VarKind
	: "mut"         { $$ = varlocalmut_new($1.span); }
	| "persistent"  { $$ = varpersistent_new($1.span); }
	| "const"       { $$ = varconst_new($1.span); }
	| %empty        { $$ = varlocal_new(span_new(0,0)); }
	;

TypeAscription
	: %empty        { $$ = unspecified_new(span_new(0,0)); }
	| NameableType  { $$ = $1; }
	;

Assignment
	: LValue "=" Expr ";" { $$ = assignment_new($1, $3, span_merge($1.span, $4.span)); }
	;

FunctionDef
	: FunctionSig Block  { $1.body = $2; $$ = $1; }
	;

FunctionSig
	: "fn" "{ident}" "(" FunctionParamSeq ")" FunctionRetType
		{ 
			$$ = functiondef_new(
				$2.repr, $4, ALLOC($6,Type), 
				block_new(stmtseq_new(NULL, span_new(0,0)),span_new(0,0)),
				span_merge($1.span, $6.span)); 
		}
	;

FunctionParamSeq
	: %empty { $$ = typedparamseq_new(NULL, span_new(0,0)); }
	| TypedIdent
		{ 
			$$ = typedparamseq_new(
					ALLOC(typedparamnode_new(ALLOC($1, TypedParam)),TypedParamNode), 
					$1.span); 
		}
	| TypedIdent "," FunctionParamSeq 
		{
			typedparamseq_push(&$3, $1);
			$3.span = span_merge($1.span, $3.span);
			$$ = $3;
		}
	;

FunctionRetType
	: %empty { $$ = unspecified_new(span_new(0,0)); }
	| ":" NameableType { $$ = $2; }
	;

Block
	: "begin" BlockEnd { $2.span = span_merge($1.span, $2.span); $$ = $2; }
	;

BlockEnd
	: StmtSeq "end" { $$ = block_new($1, span_merge($1.span, $2.span)); }
	;

StmtSeq
	: %empty { $$ = stmtseq_new(NULL, span_new(0,0)); }
	| Expr
		{ 
			$$ = stmtseq_new(
					ALLOC(stmtnode_new(ALLOC(exprstmt_new($1, $1.span), Stmt)),StmtNode), 
					$1.span); 
		}

	| Stmt StmtSeq
		{
			stmtseq_push(&$2, $1);
			$2.span = span_merge($1.span, $2.span);
			$$ = $2;
		}
	;

Stmt
	: Item        { $$ = itemstmt_new($1, $1.span); }
	| Assignment  { $$ = assignmentstmt_new($1, $1.span); }
	| ExprStmt    { $$ = $1; }
	;


Expr /* before ExprStmt to resolve reduce/reduce conflic */
	: ExprWithBlock     { $$ = $1; }
	| ExprWithoutBlock 	{ $$ = $1; }
	;

ExprStmt
	: ExprWithBlock { $$ = exprstmt_new($1, $1.span); } 
	| Expr ";" { $$ = exprstmt_new($1, $1.span); }
	;

LValue
	: "{ident}"              { $$ = var_new($1.repr, $1.span); }
	| "this"                 { $$ = this_new($1.span); }
	| LValue "." "{ident}"   
		{ 
			$$ = memberaccess_new(
				ALLOC($1, LValue), $3.repr,
				span_merge($1.span, $3.span)); 
		}
	| LValue "[" Expr "]"    
		{ 
			$$ = indexing_new(
				ALLOC($1, LValue), ALLOC($3, Expr), span_merge($1.span, $4.span)); 
		}
	| LValue "(" ExprSeq ")"
		{
			$$ = call_new(
				ALLOC($1, LValue), $3, span_merge($1.span, $4.span));
		}
	| LValue "::" "{ident}"
		{
			$$ = pathaccess_new(
				ALLOC($1, LValue), $3.repr, span_merge($1.span, $3.span));
		}
	;

TypeDef
	: "type" TypeIdent "=" DefinableType
		{
			$$ = typedef_new($2.it.ident, $4, span_merge($1.span, $4.span));
		}
	;

DefinableType
	: NameableType ";" { $$ = $1; }
	| Record           { $$ = $1; }
	| Enum             { $$ = $1; }
	;

Record
	: "record" RecordFieldSeq "end" { $$ = recordty_new($2, span_merge($1.span, $2.span)); }
	;

RecordFieldSeq
	: %empty       { $$ = typedparamseq_new(NULL, span_new(0,0)); }
	| RecordField
		{ 
			$$ = typedparamseq_new(
					ALLOC(typedparamnode_new(ALLOC($1, TypedParam)),TypedParamNode), 
					$1.span); 
		}

	| RecordField "," RecordFieldSeq
		{
			typedparamseq_push(&$3, $1);
			$3.span = span_merge($1.span, $3.span);
			$$ = $3;
		}
	;

RecordField
	: Vis TypedIdent { $$ = $2; } 
	;

TypedIdent
	: "{ident}" ":" NameableType 
		{ 
			$$ = typedparam_new(
			$1.repr, ALLOC($3, Type),
			span_merge($1.span, $3.span)); 
		}
	;

Vis
	: %empty    {}
	| "public" 	{ lerrorln("`public` visibility modifiers not implemented yet."); }
	;

RecordLiteral
	: "record" TypeIdent NamedArgSeq "end" { lerrorln("record literals not implemented yet"); }
	;

NamedArgSeq
	: %empty    {}
	| NamedArg  {}
	| NamedArg "," NamedArgSeq {}
	;

NamedArg
	: "{ident}" "=" Expr {}
	;

Enum
	: "enum" EnumVariantSeq "end" 
		{ 
			$$ = enumty_new($2, span_merge($1.span, $3.span)); 
		} 
	;

EnumVariantSeq
	: %empty { $$ = enumvariantseq_new(NULL, span_new(0,0)); }
	| EnumVariant 
		{ 
			$$ = enumvariantseq_new(
				ALLOC(enumvariantnode_new(ALLOC($1, EnumVariant)),EnumVariantNode), 
				$1.span); 
		}
	| EnumVariant "," EnumVariantSeq
		{
			enumvariantseq_push(&$3, $1);
			$3.span = span_merge($1.span, $3.span);
			$$ = $3;
		}

	;

EnumVariant
	: "{ident}"           
		{ 
			$$ = enumvariant_new(
				$1.repr, 
				typeseq_new(NULL, span_new(0,0)), 
				$1.span);
		}
	| "{ident}" "=" Tuple
		{ 
			$$ = enumvariant_new($1.repr, $3.it.tup, span_merge($1.span, $3.span));
		}
	;

TypeImpl
	: "impl" TypeIdent MethodSeq "end" 
		{
			$$ = typeimpl_new($2.it.ident, $3, span_merge($1.span, $4.span));
		}
	;

MethodSeq
	: %empty { $$ = functiondefseq_new(NULL, span_new(0,0)); }
	| MethodDef MethodSeq 
		{
			functiondefseq_push(&$2, $1);
			$2.span = span_merge($1.span, $2.span);
			$$ = $2;
		}
	;

MethodDef
	: MethodSig Block { $1.body = $2; $$ = $1; }
	;

MethodSig
	: "fn" "{ident}" "(" MethodParamSeq ")" FunctionRetType
		{ 
			$$ = functiondef_new(
				$2.repr, $4, ALLOC($6,Type), 
				block_new(stmtseq_new(NULL, span_new(0,0)),span_new(0,0)),
				span_merge($1.span, $6.span)); 
		}

	;

MethodParamSeq
	: FunctionParamSeq { $$ = $1; }
	| "this"  { lerrorln("`this` as a method parameter is not implemented yet"); }
	| "this" "," FunctionParamSeq { lerrorln("`this` as a method parameter is not implemented yet"); }
	;


ExprWithBlock
	: ForExpr   { $$ = forexpr_new($1, $1.span); }
	| LoopExpr  { $$ = loopexpr_new($1, $1.span); }
	| IfExpr    { $$ = ifexpr_new($1, $1.span); }
	| Block     { $$ = blockexpr_new($1, $1.span); }
	;

ExprWithoutBlock
	: PrimitiveExpr { $$ = $1; }
	| ReturnExpr    { $$ = $1; }
	| BreakExpr     { $$ = $1; }
	| Expr "and" Expr 
		{ 
			$$ = binopexpr_new(
				$2, ALLOC($1, Expr), ALLOC($3, Expr), 
				span_merge($1.span, $3.span)); 
		}
	| Expr "or" Expr
		{ 
			$$ = binopexpr_new(
				$2, ALLOC($1, Expr), ALLOC($3, Expr), 
				span_merge($1.span, $3.span)); 
		}
	| Expr "==" Expr
		{ 
			$$ = binopexpr_new(
				$2, ALLOC($1, Expr), ALLOC($3, Expr), 
				span_merge($1.span, $3.span)); 
		}
	| Expr "!=" Expr
		{ 
			$$ = binopexpr_new(
				$2, ALLOC($1, Expr), ALLOC($3, Expr), 
				span_merge($1.span, $3.span)); 
		}
	| Expr "<=" Expr
		{ 
			$$ = binopexpr_new(
				$2, ALLOC($1, Expr), ALLOC($3, Expr), 
				span_merge($1.span, $3.span)); 
		}
	| Expr ">=" Expr
		{ 
			$$ = binopexpr_new(
				$2, ALLOC($1, Expr), ALLOC($3, Expr), 
				span_merge($1.span, $3.span)); 
		}
	| Expr "<" Expr
		{ 
			$$ = binopexpr_new(
				$2, ALLOC($1, Expr), ALLOC($3, Expr), 
				span_merge($1.span, $3.span)); 
		}
	| Expr ">" Expr
		{ 
			$$ = binopexpr_new(
				$2, ALLOC($1, Expr), ALLOC($3, Expr), 
				span_merge($1.span, $3.span)); 
		}
	| Expr "+" Expr
		{ 
			$$ = binopexpr_new(
				$2, ALLOC($1, Expr), ALLOC($3, Expr), 
				span_merge($1.span, $3.span)); 
		}
	| Expr "-" Expr
		{ 
			$$ = binopexpr_new(
				$2, ALLOC($1, Expr), ALLOC($3, Expr), 
				span_merge($1.span, $3.span)); 
		}
	| Expr "*" Expr
		{ 
			$$ = binopexpr_new(
				$2, ALLOC($1, Expr), ALLOC($3, Expr), 
				span_merge($1.span, $3.span)); 
		}
	| Expr "/" Expr
		{ 
			$$ = binopexpr_new(
				$2, ALLOC($1, Expr), ALLOC($3, Expr), 
				span_merge($1.span, $3.span)); 
		}
	| "(" Expr ")" { $$ = $2; }
	;

PrimitiveExpr
	: LValue  { $$ = lvalueexpr_new($1, $1.span); }
	| Literal { $$ = litexpr_new($1, $1.span); }
	;

ReturnExpr
	: "return" Expr { $$ = retexpr_new(ALLOC($2, Expr), span_merge($1.span, $2.span));}
	| "return"      { $$ = retexpr_new(NULL, $1.span);}
	;

BreakExpr
	: "break" Expr  { $$ = breakexpr_new(ALLOC($2, Expr), span_merge($1.span, $2.span));}
	| "break"       { $$ = breakexpr_new(NULL, $1.span);}
	;

ExprSeq
	: %empty { $$ = exprseq_new(NULL, span_new(0,0)); }
	| Expr	
		{ 
			$$ = exprseq_new(
				ALLOC(exprnode_new(ALLOC($1, Expr)),ExprNode), $1.span); 
		}
	| Expr "," ExprSeq
		{
			exprseq_push(&$3, $1);
			$3.span = span_merge($1.span, $3.span);
			$$ = $3;
		}
	;

Literal
	: "{number}"      { $$ = $1; }
	| "{string}"      { $$ = $1; }
	| "true"          { $$ = $1; }
	| "false"         { $$ = $1; }
	| "[" ExprSeq "]" { lerrorln("array literals not implemented yet"); }
	| "(" ExprSeq ")" { lerrorln("tuple literals not implemented yet"); }
	| RecordLiteral   {}
	;

LoopExpr
	: "loop" BlockEnd { $$ = loop_new($2, span_merge($1.span, $2.span)); }
	;

ForExpr
	: "for" "{ident}" "in" Expr "do" BlockEnd
		{
			$$ = for_new($2.repr, ALLOC($4, Expr), $6, span_merge($1.span, $6.span));
		}
	;

IfExpr
	: "if" IfCond { $2.span = span_merge($1.span, $2.span); $$ = $2; }
	;

IfCond
	: Expr "then" StmtSeq IfAlts 
		{
			Span end_span = $4 != NULL ? $4->span : $3.span;
			$$ = if_new(ALLOC($1, Expr), block_new($3, $3.span), $4, span_merge($1.span, end_span));
		}
	;

IfAlts
	: "elif" IfCond { lerrorln("`elif` branches not implemented yet"); $$ = NULL; }
	| IfEnd { $$ = $1; } 
	;

IfEnd
	: "else" BlockEnd { $$ = ALLOC($2, Block); }
	| "end" { $$ = NULL; }
	;

Program
	: ItemSeq { *result = program_new($1, $1.span); }
	;

ItemSeq
	: %empty { $$ = itemseq_new(NULL, span_new(0,0)); }
	| Item ItemSeq
		{
			itemseq_push(&$2, $1);
			$2.span = span_merge($1.span, $2.span);
			$$ = $2;
		}

	;

Item
	: TypeDef     { $$ = typedefitem_new($1, $1.span); }
	| TypeImpl    { $$ = typeimplitem_new($1, $1.span); }
	| FunctionDef { $$ = functiondefitem_new($1, $1.span); }
	| VariableDef { $$ = vardefitem_new($1, $1.span); }
	;

%%


int yyerror(YYLTYPE* loc, char const* msg) { 
	(void) loc; (void) msg;
	return 0; 
}

extern int sprintf(char* dest, char const* fmt, ...);
extern usize strlen(char const* src);

// TODO: don't use logging but create some
//       sort of diagnostic builder
static int yyreport_syntax_error(
	yypcontext_t const* ctx, 
	CompilerContext* comp_ctx,
	yyscan_t scanner,
	Program* result) 
{ 
	(void) scanner; (void) result;
	yysymbol_kind_t found = yypcontext_token(ctx);
	yysymbol_kind_t expected[5];
	i32 n_expected = yypcontext_expected_tokens(ctx, expected, 5);

	char* expected_string = (char*) malloc(10*sizeof(char));
	expected_string[0] = '\0';
	for (i32 i = 0; i < n_expected; ++i) {
		char const* name = yysymbol_name(expected[i]);
		expected_string = realloc(
			expected_string, 
			sizeof(char) * (strlen(expected_string) + strlen(name) + 24));	

		sprintf(
			expected_string, "%s\"%s\"%s%s", 
			color_bold_yellow(), name, color_reset(),
			n_expected == 1 ? "" : (i == n_expected ? " or" : ", ")
		);
	}

	Location loc = span_to_location(comp_ctx, *yypcontext_location(ctx));
	
	lerrorln("at %s%s:%d:%d%s, unexpected %s\"%s\"%s%s%s",
		color_bold_white(),	
		comp_ctx->path,
		loc.start_line,
		loc.start_col,
		color_reset(),
		color_bold_yellow(),
		yysymbol_name(found),
		color_reset(),
		n_expected > 0 ? ", expected " : "",
		expected_string);
	return 0;
}
