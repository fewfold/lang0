#pragma once

#ifndef TYPES_H
#define TYPES_H

#define i64 long int
#define i32 int
#define i16 short
#define i8  char

#define u64   unsigned i64
#define u32   unsigned i32
#define u16   unsigned i16
#define u8    unsigned i8
#define usize __SIZE_TYPE__

#define bool _Bool
#define false 0
#define true 1

#endif /* TYPES_H */
