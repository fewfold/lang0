#include "diagnostics.h"

#include <math.h>

extern usize strlen(char const*);
extern char* strndup(char const*, usize);
extern void* memset(void*, i32, usize);

Span span_new(usize start, usize len) {
	Span it = { .start = start, .len = len };
	return it;
}

Span span_merge(Span a, Span b) {
	Span merged;
	Span before = a.start < b.start ? a : b;
	Span after = a.start < b.start ? b : a;
	if (before.start + before.len > after.start + after.len) {
		merged.start = before.start;
		merged.len = before.len;
	} else {
		merged.start = before.start;
		merged.len = after.len + after.start - before.start;
	}

	return merged;
}
extern i32 sprintf(char*, char const*, ...);

char const* span_to_string(Span const* span) {
	char* res = (char*) malloc(32 * sizeof(char));
	sprintf(res, "(span %lu %lu)", span->start, span->len);
	return res;
}

Location location_new(
	usize start_line, usize start_col, usize end_line, usize end_col
) {
	Location it = { .start_line = start_line,
		            .start_col = start_col,
		            .end_line = end_line,
		            .end_col = end_col };
	return it;
}

Location span_to_location(CompilerContext const* ctx, Span span) {
	Location loc
		= { .start_line = 1, .start_col = 1, .end_line = 1, .end_col = 1 };
	usize* line = &loc.start_line;
	usize* col = &loc.start_col;
	for (usize i = 0; i < span.start + span.len; ++i) {
		if (i == span.start) {
			loc.end_line = *line;
			loc.end_col = *col;

			line = &loc.end_line;
			col = &loc.end_col;
		}

		if (ctx->src[i] == '\n') {
			*line += 1;
			*col = 1;
		} else {
			*col += 1;
		}
	}

	return loc;
}

char const* diagnostic_get_lines(Diagnostic dnostic) {
	Location loc = span_to_location(dnostic.ctx, dnostic.span);
	usize begin = 0;
	usize nbytes = 0;
	usize curr_line_number = 1;
	for (char const* c = dnostic.ctx->src; *c != '\0'; ++c, ++nbytes) {
		if (*c == '\n') {
			curr_line_number += 1;

			if (curr_line_number == loc.start_line) { begin = nbytes + 1; }

			if (curr_line_number == loc.end_line + 1) { break; }
		}
	}

	char const* lines = strndup(dnostic.ctx->src + begin, nbytes - begin);
	return lines;
}

char const* diagnostic_hightlight_lines(Diagnostic dnostic) {
	char const* highlight = diagnostic_get_lines(dnostic);
	usize bytes = strlen(highlight);

	if (dnostic.span.start >= 1)
		memset((void*) highlight, ' ', dnostic.span.start - 1);
	memset((void*) highlight + dnostic.span.start, '^', dnostic.span.len);
	if (dnostic.span.len < bytes)
		memset(
			(void*) highlight + dnostic.span.len,
			' ',
			bytes - (dnostic.span.start + dnostic.span.len + 1)
		);

	return highlight;
}

FILE* severity_to_stream(Severity sev) {
	FILE* io;
	switch (sev) {
		case SeverityError:
		case SeverityWarning: io = stderr; break;
	}
	return io;
}

char const* severity_to_string(Severity sev) {
	char const* string;
	switch (sev) {
		case SeverityError: string = "error"; break;
		case SeverityWarning: string = "warning"; break;
	}
	return string;
}

char const* severity_to_color(Severity sev) {
	char const* string;
	switch (sev) {
		case SeverityError: string = color_bold_red(); break;
		case SeverityWarning: string = color_bold_yellow(); break;
	}
	return string;
}

Diagnostic diagnostic_new(
	CompilerContext const* ctx, Severity sev, Span span, char const* msg
) {
	Diagnostic it = { .ctx = ctx, .sev = sev, .span = span, .msg = msg };
	return it;
}

extern int fprintf(FILE*, char const*, ...);

#define NDIGITS(n) (floor(log10(n)) + 1)
#define MAX(a, b)  ((a) > (b) ? (a) : (b))

void diagnostic_display(Diagnostic dnostic) {
	FILE* io = severity_to_stream(dnostic.sev);
	Location loc = span_to_location(dnostic.ctx, dnostic.span);
	fprintf(
		io,
		"%s%s:%lu:%lu: ",
		color_bold_white(),
		dnostic.ctx->path,
		loc.start_line,
		loc.start_col
	);
	fprintf(
		io,
		"%s%s:%s %s\n",
		severity_to_color(dnostic.sev),
		severity_to_string(dnostic.sev),
		color_reset(),
		dnostic.msg
	);
	usize n_digits = MAX(NDIGITS(loc.start_line), NDIGITS(loc.end_line));
	(void) n_digits;

	char const* lines = diagnostic_get_lines(dnostic);
	char const* highlight = diagnostic_hightlight_lines(dnostic);
	fprintf(io, "%s", lines);
	fprintf(io, "%s", highlight);
	// char const** highlights = ctx_create_highlight(dnostic.ctx,loc);

	// for (usize i = 0; i < loc.end_line - loc.start_line + 1; ++i) {
	//	fprintf(
	//		io,
	//		"  %*lu | %s\n  %*s | %s\n",
	//		(i32) n_digits,
	//		loc.start_line + i,
	//		// TODO: strreplace(lines[i], '\t', "    ") or something
	//		lines[i],
	//		(i32) n_digits,
	//		"",
	//		highlights[i]
	//	);
	// }

	free((void*) lines);
}

#undef NDIGITS
#undef MAX
