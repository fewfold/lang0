#pragma once

#ifndef INTERNER_H
#define INTERNER_H

#include "prelude.h"

struct Interner {
	char const** symbols;
	usize cap;
	usize len;
};
typedef struct Interner Interner;

typedef usize Symbol;
char const* symbol_to_string(Symbol const* symbol);

void interner_init(Interner* interner);
Symbol intern(Interner* interner, char const* s);
char const* retreive(Interner const* interner, Symbol s);
void interner_free(Interner* interner);

void ginterner_init();
Symbol ginterner_intern(char const* s);
char const* ginterner_retreive(Symbol s);
void ginterner_free();

char* interner_to_string(Interner const* interner);
char* ginterner_to_string();

#endif /* INTERNER_H */
