#include "llog.h"

#include <stdarg.h> /* for va_list and va_start */

#include "io.h"
#include "memory.h"

bool LOG_COLORS = true;
LogLevel GLOBAL_LOGLEVEL = LogLevelWarn;

extern int fprintf(FILE*, char const*, ...);
extern int vfprintf(FILE*, char const*, va_list);

static inline FILE* loglevel_to_stream(LogLevel level) {
	FILE* io;

	switch (level) {
		case LogLevelError:
		case LogLevelWarn: io = stderr; break;
		case LogLevelDebug:
		case LogLevelInfo:
		case LogLevelTrace: io = stdout; break;
	}

	return io;
}

char const* color_bold_red() { return LOG_COLORS ? "\033[1;91m" : ""; }
char const* color_bold_yellow() { return LOG_COLORS ? "\033[1;93m" : ""; }
char const* color_bold_green() { return LOG_COLORS ? "\033[1;92m" : ""; }
char const* color_bold_blue() { return LOG_COLORS ? "\033[1;94m" : ""; }
char const* color_bold_white() { return LOG_COLORS ? "\033[1;97m" : ""; }
char const* color_reset() { return LOG_COLORS ? "\033[0m" : ""; }

char const* loglevel_to_color(LogLevel level) {
	switch (level) {
		case LogLevelError: return color_bold_red();
		case LogLevelWarn: return color_bold_yellow();
		case LogLevelDebug: return color_bold_green();
		case LogLevelInfo: return color_bold_blue();
		case LogLevelTrace: return color_bold_white();
	}
	return "";
}

char const* loglevel_to_header(LogLevel level) {
	switch (level) {
		case LogLevelError: return "error:";
		case LogLevelWarn: return "warning:";
		case LogLevelDebug: return "debug:";
		case LogLevelInfo: return "info:";
		case LogLevelTrace: return "trace:";
	}
	return "";
}

static inline void
	_vlog(LogLevel level, char const* fmt, va_list args, bool newline) {
	if (level > GLOBAL_LOGLEVEL) return;

	FILE* io = loglevel_to_stream(level);
	fprintf(
		io,
		"%s%s%s ",
		loglevel_to_color(level),
		loglevel_to_header(level),
		color_reset()
	);
	vfprintf(io, fmt, args);
	if (newline) fprintf(io, "\n");
}

static inline void vlog(LogLevel level, char const* fmt, va_list args) {
	_vlog(level, fmt, args, false);
}

static inline void vlogln(LogLevel level, char const* fmt, va_list args) {
	_vlog(level, fmt, args, true);
}

void llog(LogLevel level, char const* fmt, ...) {
	va_list args;
	va_start(args, fmt);
	vlog(level, fmt, args);
}

void llogln(LogLevel level, char const* fmt, ...) {
	va_list args;
	va_start(args, fmt);
	vlogln(level, fmt, args);
}

void lerror(char const* fmt, ...) {
	va_list args;
	va_start(args, fmt);
	vlog(LogLevelError, fmt, args);
}

void lerrorln(char const* fmt, ...) {
	va_list args;
	va_start(args, fmt);
	vlogln(LogLevelError, fmt, args);
}

void lwarn(char const* fmt, ...) {
	va_list args;
	va_start(args, fmt);
	vlog(LogLevelWarn, fmt, args);
}

void lwarnln(char const* fmt, ...) {
	va_list args;
	va_start(args, fmt);
	vlogln(LogLevelWarn, fmt, args);
}

void ldebug(char const* fmt, ...) {
	va_list args;
	va_start(args, fmt);
	vlog(LogLevelDebug, fmt, args);
}

void ldebugln(char const* fmt, ...) {
	va_list args;
	va_start(args, fmt);
	vlogln(LogLevelDebug, fmt, args);
}

void linfo(char const* fmt, ...) {
	va_list args;
	va_start(args, fmt);
	vlog(LogLevelInfo, fmt, args);
}

void linfoln(char const* fmt, ...) {
	va_list args;
	va_start(args, fmt);
	vlogln(LogLevelInfo, fmt, args);
}

void ltrace(char const* fmt, ...) {
	va_list args;
	va_start(args, fmt);
	vlog(LogLevelTrace, fmt, args);
}

void ltraceln(char const* fmt, ...) {
	va_list args;
	va_start(args, fmt);
	vlogln(LogLevelTrace, fmt, args);
}

extern usize strlen(char const* str);

void lassert(
	char const* file, i32 line, char const* cond, char const* fmt, ...
) {
	lerror(
		"assertion %s`%s`%s failed at %s%s:%d%s",
		color_bold_yellow(),
		cond,
		color_reset(),
		color_bold_white(),
		file,
		line,
		color_reset()
	);

	FILE* io = loglevel_to_stream(LogLevelError);

	if (strlen(fmt) > 0) {
		va_list args;
		va_start(args, fmt);

		fprintf(io, " with %s\"", color_bold_yellow());
		vfprintf(io, fmt, args);
		fprintf(io, "\"%s", color_reset());
	}

	fprintf(io, "\n");
}
