#include "interner.h"

extern i32 strcmp(char const*, char const*);
extern char* strdup(char const*);
extern char* strcpy(char*, char const*);
extern usize strlen(char const*);

static inline void grow(Interner* interner) {
	ltraceln("growing interner");
	interner->cap *= 2;
	interner->symbols = (char const**) realloc(
		interner->symbols, interner->cap * sizeof(char const*)
	);
	assertm(interner->symbols != NULL, "%m");
}

static Interner GLOBAL_INTERNER;

char const* symbol_to_string(Symbol const* symbol) {
	return ginterner_retreive(*symbol);
}

void interner_init(Interner* interner) {
	interner->cap = 8;
	interner->len = 0;
	interner->symbols
		= (char const**) malloc(interner->cap * sizeof(char const*));
	assertm(interner->symbols != NULL, "%m");
}

void ginterner_init() { interner_init(&GLOBAL_INTERNER); }

Symbol intern(Interner* interner, char const* s) {
	for (usize i = 0; i < interner->len; ++i) {
		if (strcmp(s, interner->symbols[i]) == 0) { return i; }
	}

	if (interner->cap == interner->len) { grow(interner); }

	interner->symbols[interner->len] = strdup(s);
	interner->len += 1;

	return interner->len - 1;
}

Symbol ginterner_intern(char const* s) { return intern(&GLOBAL_INTERNER, s); }

char const* retreive(Interner const* interner, Symbol s) {
	assertm(s < interner->len, "symbol %lu does not exist", s);
	return interner->symbols[s];
}

char const* ginterner_retreive(Symbol s) {
	return retreive(&GLOBAL_INTERNER, s);
}

void interner_free(Interner* interner) {
	for (usize i = 0; i < interner->len; ++i) {
		free((void*) interner->symbols[i]);
	}

	free((void*) interner->symbols);

	interner->cap = 0;
	interner->len = 0;
}

void ginterner_free() { interner_free(&GLOBAL_INTERNER); }

char* interner_to_string(Interner const* interner) {
	usize total_len = 0;
	for (usize i = 0; i < interner->len; ++i) {
		total_len += 1 + strlen(interner->symbols[i]) + 1 + 1 + 1;
	}
	total_len += 1;
	char* dump = (char*) malloc(total_len * sizeof(char));
	char* pdump = dump;
	*pdump++ = '[';
	for (usize i = 0; i < interner->len; ++i) {
		*pdump++ = '\"';
		pdump = strcpy(pdump, interner->symbols[i]);
		pdump += strlen(interner->symbols[i]);
		*pdump++ = '\"';
		if (i < interner->len - 1) {
			*pdump++ = ',';
			*pdump++ = ' ';
		}
	}
	*pdump++ = ']';
	*pdump++ = '\0';
	return dump;
}

char* ginterner_to_string() { return interner_to_string(&GLOBAL_INTERNER); }
