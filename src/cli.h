#pragma once

#ifndef CLI_H
#define CLI_H

#include "prelude.h"

struct App {
	char const* src;
	bool bison_debug;
};
typedef struct App App;

App make_app_from_args(i32 argc, char** argv);

#endif /* CLI_H */
