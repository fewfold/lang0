#pragma once

#ifndef ASSERT_H
#define ASSERT_H

#include "llog.h"
#include "types.h"

extern void exit(i32 code);

#define assertm(cond, ...)                                                     \
	do {                                                                       \
		if (!(cond)) {                                                         \
			lassert(__FILE__, __LINE__, #cond, __VA_ARGS__);                   \
			exit(1);                                                           \
		}                                                                      \
	} while (false)

#undef assert
#define assert(cond) assertm(cond, "")

#undef panic
#define panic(msg)                                                             \
	do {                                                                       \
		lerrorln(                                                              \
			"panicked with %s`%s`%s at %s" __FILE__ ":%d%s",                   \
			color_bold_yellow(),                                               \
			(msg),                                                             \
			color_reset(),                                                     \
			color_bold_white(),                                                \
			__LINE__,                                                          \
			color_reset()                                                      \
		);                                                                     \
		exit(1);                                                               \
	} while (false)

#undef unreachable
#define unreachable() panic("unreachable reached!")

#endif /* ASSERT_H */
