#include "cli.h"

#include <getopt.h>

#include "prelude.h"

extern i32 atoi(char const*);

struct option long_options[] = {
	{	   "loud", optional_argument, 0, 'l'},
	{	  "plain",       no_argument, 0, 'p'},
	{"bison-debug",       no_argument, 0, 'y'},
	{			0,				 0, 0,   0},
};

struct Description {
	char* short_name;
	char const* desc;
};
typedef struct Description Description;

Description descriptions[] = {
	{"l",			   "enable tracing"},
	{"p",			   "disable colors"},
	{"y", "enable debug traces of bison"}
};

char const* short_options = "l::py";

App make_app_from_args(i32 argc, char** argv) {
	App app = { .src = NULL };

	loop {
		i32 option_index = 0;
		i32 c = getopt_long(
			argc, argv, short_options, long_options, &option_index
		);

		if (c == -1) break;

		switch (c) {
			case 'l':
				if (optarg) {
					GLOBAL_LOGLEVEL = (LogLevel) atoi(optarg);
				} else {
					GLOBAL_LOGLEVEL = LogLevelTrace;
				}
				break;
			case 'p': LOG_COLORS = false; break;
			case 'y': app.bison_debug = true; break;
		}
	}

	if (optind < argc) { app.src = argv[optind]; }

	return app;
}
