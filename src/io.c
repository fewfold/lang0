#include "io.h"

#include "llog.h"
#include "memory.h"

char* read_file(char const* filename) {
	ltraceln("reading file `%s`...", filename);

	FILE* input = fopen(filename, "r");
	if (!input) return NULL;

	fseek(input, 0, SEEK_END);
	i64 file_size = ftell(input);
	fseek(input, 0, SEEK_SET);

	char* content = (char*) malloc(file_size * sizeof(char));
	if (!content) return NULL;

	fread(content, sizeof(char), file_size, input);

	fclose(input);

	ltraceln("successfully read `%s`", filename);
	return content;
}
