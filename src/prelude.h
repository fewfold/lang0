#pragma once

#ifndef PRELUDE_H
#define PRELUDE_H

#include "assert.h"
#include "bits.h"
#include "interner.h"
#include "io.h"
#include "llog.h"
#include "memory.h"
#include "types.h"

#endif /* PRELUDE_H */
