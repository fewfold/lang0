#include "ast.h"
#include "cli.h"
#include "diagnostics.h"
#include "prelude.h"

// clang-format off
// the order of the next two includes are important!
#include "parser.h"
#include "lexer.h"
// clang-format on

i32 main(i32 argc, char** argv) {
	App app = make_app_from_args(argc, argv);
	assertm(app.src != NULL, "No input file provided");

	ginterner_init();

	char* content = read_file(app.src);
	assertm(content != NULL, "%m");

	CompilerContext ctx = { .path = app.src, .src = content };

	yyscan_t scanner;
	YY_BUFFER_STATE buffer;
	yylex_init(&scanner);
	buffer = yy_scan_string(content, scanner);

	ldebugln("parsing file `%s`...", app.src);
	if (app.bison_debug) yydebug = 1;

	Program result;
	usize success = yyparse(&ctx, scanner, &result);

	if (success > 0) {
		ldebugln("failed to parse `%s`", app.src);
	} else {
		ldebugln("successfully parsed `%s`", app.src);
		ldebugln("%s", program_to_string(&result));
	}

	yy_delete_buffer(buffer, scanner);
	yylex_destroy(scanner);
	ginterner_free();
	free(content);
	return 0;
}
