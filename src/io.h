#pragma once

#ifndef IO_H
#define IO_H

#include "types.h"

struct _IO_FILE;
typedef struct _IO_FILE FILE;

#define EOF (-1)

extern FILE* stdin;
extern FILE* stderr;
extern FILE* stdout;

extern FILE* fopen(char const* restrict filename, char const* restrict mode);
extern i32 fclose(FILE* file);

#define SEEK_SET 0
#define SEEK_CUR 1
#define SEEK_END 2

extern i32 fseek(FILE* file, i64 offset, i32 origin);
extern i64 ftell(FILE* file);
extern usize fread(void* buffer, usize size, usize length, FILE* file);

char* read_file(char const* filename);

#endif /* IO_H */
