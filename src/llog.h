#pragma once

#ifndef LLOG_H
#define LLOG_H

#include "types.h"

enum LogLevel {
	LogLevelError = 0,
	LogLevelWarn = 1,
	LogLevelDebug = 2,
	LogLevelInfo = 3,
	LogLevelTrace = 4,
};
typedef enum LogLevel LogLevel;

extern bool LOG_COLORS;
extern LogLevel GLOBAL_LOGLEVEL;

char const* color_bold_red();
char const* color_bold_yellow();
char const* color_bold_green();
char const* color_bold_blue();
char const* color_bold_white();
char const* color_reset();

char const* loglevel_to_color(LogLevel level);
char const* loglevel_to_header(LogLevel level);

void llog(LogLevel level, char const* fmt, ...);
void llogln(LogLevel level, char const* fmt, ...);

void lerror(char const* fmt, ...);
void lerrorln(char const* fmt, ...);

void lwarn(char const* fmt, ...);
void lwarnln(char const* fmt, ...);

void ldebug(char const* fmt, ...);
void ldebugln(char const* fmt, ...);

void linfo(char const* fmt, ...);
void linfoln(char const* fmt, ...);

void ltrace(char const* fmt, ...);
void ltraceln(char const* fmt, ...);

void lassert(
	char const* file, i32 line, char const* cond, char const* fmt, ...
);

#endif /* LLOG_H */
