%{
#include "parser.h"
#include "llog.h"
#include "interner.h"
#include "ast.h"


char str_buf[512];
char* str_ptr;

#define UPDATE_LOC() do { yylloc->start += yylloc->len; yylloc->len = yyleng; } while (0)
#define TOKEN_RETURN(kind) UPDATE_LOC(); yylval->kind = token_new(*yylloc, ginterner_intern(yytext)); return (kind);
#define LIT_RETURN(kind, constructor) UPDATE_LOC(); yylval->kind = constructor##_new(ginterner_intern(yytext), *yylloc); return kind;
#define TY_RETURN(kind, constructor) UPDATE_LOC(); yylval->kind = constructor##_new(*yylloc); return kind;


%}

%option noyywrap reentrant bison-bridge bison-locations batch

alphabetic   [a-zA-Z]
numeric      [0-9]
alphanumeric {alphabetic}|{numeric}
whitespace   [ \n\r\t]

%x STRING COMMENT

%%

{whitespace} UPDATE_LOC();

"//".* UPDATE_LOC();

"/*"  BEGIN(COMMENT); UPDATE_LOC();
<COMMENT>{
"*/"     BEGIN(INITIAL); yylloc->len += 2;
[^*\n]+  yylloc->len += yyleng;
"*"      yylloc->len += yyleng;
}

(0|[1-9][0-9]*)          LIT_RETURN(Kind_Number, intlit);
(0|[1-9][0-9]*)\.[0-9]+  LIT_RETURN(Kind_Number, intlit);

"true" 	                 LIT_RETURN(Kind_True, truelit);
"false"                  LIT_RETURN(Kind_False, falselit);
"this"                   LIT_RETURN(Kind_This, thislit);

"char" 	TY_RETURN(Kind_Char, charty);
"f32" 	TY_RETURN(Kind_F32, f32ty);
"f64"   TY_RETURN(Kind_F64, f64ty);
"u8" 	TY_RETURN(Kind_U8, u8ty);
"u16"   TY_RETURN(Kind_U16, u16ty);
"u32"   TY_RETURN(Kind_U32, u32ty);
"u64"   TY_RETURN(Kind_U64, u64ty);
"usize" TY_RETURN(Kind_Usize, usizety);
"i8" 	TY_RETURN(Kind_I8, i8ty);
"i16"   TY_RETURN(Kind_I16, i16ty);
"i32"   TY_RETURN(Kind_I32, i32ty);
"i64"   TY_RETURN(Kind_I64, i64ty);
"isize" TY_RETURN(Kind_Isize, isizety);
"bool"	TY_RETURN(Kind_Bool, boolty);

"type" 	 TOKEN_RETURN(Kind_Type);
"=" 	 TOKEN_RETURN(Kind_Eq);
"record" TOKEN_RETURN(Kind_Record);
"enum" 	 TOKEN_RETURN(Kind_Enum);
"end" 	 TOKEN_RETURN(Kind_End);
"public" TOKEN_RETURN(Kind_Public);
"fn" 	 TOKEN_RETURN(Kind_Fn);
"begin"  TOKEN_RETURN(Kind_Begin);
"impl"   TOKEN_RETURN(Kind_Impl);
"return" TOKEN_RETURN(Kind_Return);
"break"  TOKEN_RETURN(Kind_Break);


"let" 	TOKEN_RETURN(Kind_Let);
"mut" 	TOKEN_RETURN(Kind_Mut);
"const" TOKEN_RETURN(Kind_Const);
"persistent" TOKEN_RETURN(Kind_Persistent);

"loop" 	TOKEN_RETURN(Kind_Loop);
"for" 	TOKEN_RETURN(Kind_For);
"do"    TOKEN_RETURN(Kind_Do);
"in" 	TOKEN_RETURN(Kind_In);
"if" 	TOKEN_RETURN(Kind_If);
"then" 	TOKEN_RETURN(Kind_Then);
"else" 	TOKEN_RETURN(Kind_Else);
"elif" 	TOKEN_RETURN(Kind_Elif);
"and" 	TOKEN_RETURN(Kind_And);
"or" 	TOKEN_RETURN(Kind_Or);

"[" 	TOKEN_RETURN(Kind_LBracket);
"]" 	TOKEN_RETURN(Kind_RBracket);
"(" 	TOKEN_RETURN(Kind_LParen);
")" 	TOKEN_RETURN(Kind_RParen);
"," 	TOKEN_RETURN(Kind_Comma);
":" 	TOKEN_RETURN(Kind_Colon);
"." 	TOKEN_RETURN(Kind_Dot);
";"     TOKEN_RETURN(Kind_Semi);
"::"    TOKEN_RETURN(Kind_ColonColon);

"==" 	TOKEN_RETURN(Kind_EqEq);
"!=" 	TOKEN_RETURN(Kind_BangEq);
"<=" 	TOKEN_RETURN(Kind_LtEq);
">=" 	TOKEN_RETURN(Kind_GtEq);
"<" 	TOKEN_RETURN(Kind_Lt);
">" 	TOKEN_RETURN(Kind_Gt);
"+" 	TOKEN_RETURN(Kind_Plus);
"-" 	TOKEN_RETURN(Kind_Minus);
"*" 	TOKEN_RETURN(Kind_Star);
"/" 	TOKEN_RETURN(Kind_Slash);
"&"     TOKEN_RETURN(Kind_Amp);

({alphabetic}|_)({alphanumeric}|_)* TOKEN_RETURN(Kind_Ident);

\" { 
	BEGIN(STRING); UPDATE_LOC();
	str_ptr = str_buf; 
	*str_ptr++ = '\"'; 
}

<STRING>{
[^"] { 
	yylloc->len += 1;
	*str_ptr++ = yytext[0]; 
}

\" { 
	BEGIN(INITIAL); 
	yylloc->len += 1;
	*str_ptr++ = '\"';
	*str_ptr = '\0'; 
	yylval->Kind_String = strlit_new(ginterner_intern(str_buf), *yylloc); 
	return Kind_String; 
}
}

. UPDATE_LOC();

%%
