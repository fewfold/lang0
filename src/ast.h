/********************************************************************
 * This file was generated with the `tools/generate_ast.py` script. *
 * DO NOT MODIFY!                                                   *
 ********************************************************************/

#pragma once
#ifndef AST_H
#define AST_H

#include "diagnostics.h"
#include "prelude.h"

struct Token;
struct TypedParam;
struct TypeSeq;
struct TypeNode;
struct TypedParamSeq;
struct TypedParamNode;
struct EnumVariant;
struct EnumVariantSeq;
struct EnumVariantNode;
struct Type;
struct TypeDef;
struct Literal;
struct StmtSeq;
struct StmtNode;
struct Block;
struct ExprSeq;
struct ExprNode;
struct LValue;
struct For;
struct Loop;
struct If;
struct Expr;
struct Assignment;
struct FunctionDef;
struct FunctionDefSeq;
struct FunctionDefNode;
struct TypeImpl;
struct VarDefKind;
struct VarDef;
struct Item;
struct Stmt;
struct ItemSeq;
struct ItemNode;
struct Program;
struct Token {
	Span span;
	Symbol repr;
};
typedef struct Token Token;

struct Token token_new(Span span, Symbol repr);
char* token_to_string(struct Token const* this);
struct TypedParam {
	Symbol name;
	struct Type* type;
	Span span;
};
typedef struct TypedParam TypedParam;

struct TypedParam typedparam_new(Symbol name, struct Type* type, Span span);
char* typedparam_to_string(struct TypedParam const* this);
struct TypeSeq {
	struct TypeNode* head;
	Span span;
};
typedef struct TypeSeq TypeSeq;

struct TypeSeq typeseq_new(struct TypeNode* head, Span span);
void typeseq_push(struct TypeSeq* seq, struct Type el);
struct Type* typeseq_get(struct TypeSeq* seq, usize i);
char const* typeseq_to_string(struct TypeSeq const* this);
struct TypeNode {
	struct Type* val;
	struct TypeNode* next;
};
typedef struct TypeNode TypeNode;

struct TypeNode typenode_new(struct Type* val);
struct Type* typenode_get(struct TypeNode* node, usize i);
;
struct TypedParamSeq {
	struct TypedParamNode* head;
	Span span;
};
typedef struct TypedParamSeq TypedParamSeq;

struct TypedParamSeq typedparamseq_new(struct TypedParamNode* head, Span span);
void typedparamseq_push(struct TypedParamSeq* seq, struct TypedParam el);
struct TypedParam* typedparamseq_get(struct TypedParamSeq* seq, usize i);
char const* typedparamseq_to_string(struct TypedParamSeq const* this);
struct TypedParamNode {
	struct TypedParam* val;
	struct TypedParamNode* next;
};
typedef struct TypedParamNode TypedParamNode;

struct TypedParamNode typedparamnode_new(struct TypedParam* val);
struct TypedParam* typedparamnode_get(struct TypedParamNode* node, usize i);
;
struct EnumVariant {
	Symbol name;
	struct TypeSeq tup;
	Span span;
};
typedef struct EnumVariant EnumVariant;

struct EnumVariant enumvariant_new(Symbol name, struct TypeSeq tup, Span span);
char* enumvariant_to_string(struct EnumVariant const* this);
struct EnumVariantSeq {
	struct EnumVariantNode* head;
	Span span;
};
typedef struct EnumVariantSeq EnumVariantSeq;

struct EnumVariantSeq
	enumvariantseq_new(struct EnumVariantNode* head, Span span);
void enumvariantseq_push(struct EnumVariantSeq* seq, struct EnumVariant el);
struct EnumVariant* enumvariantseq_get(struct EnumVariantSeq* seq, usize i);
char const* enumvariantseq_to_string(struct EnumVariantSeq const* this);
struct EnumVariantNode {
	struct EnumVariant* val;
	struct EnumVariantNode* next;
};
typedef struct EnumVariantNode EnumVariantNode;

struct EnumVariantNode enumvariantnode_new(struct EnumVariant* val);
struct EnumVariant* enumvariantnode_get(struct EnumVariantNode* node, usize i);
;
struct Type {
	Span span;
	enum {
		CharTy,
		F32Ty,
		F64Ty,
		U8Ty,
		U16Ty,
		U32Ty,
		U64Ty,
		UsizeTy,
		I8Ty,
		I16Ty,
		I32Ty,
		I64Ty,
		ISizeTy,
		BoolTy,
		Unspecified,
		NamedTy,
		ArrayTy,
		TupleTy,
		RecordTy,
		EnumTy,
	} tag;
	union {
		struct {
			Symbol ident;
		};
		struct {
			struct Type* type;
			struct Expr* n;
		};
		struct {
			struct TypeSeq tup;
		};
		struct {
			struct TypedParamSeq fields;
		};
		struct {
			struct EnumVariantSeq variants;
		};
	} it;
};
typedef struct Type Type;

struct Type charty_new(Span span);
struct Type f32ty_new(Span span);
struct Type f64ty_new(Span span);
struct Type u8ty_new(Span span);
struct Type u16ty_new(Span span);
struct Type u32ty_new(Span span);
struct Type u64ty_new(Span span);
struct Type usizety_new(Span span);
struct Type i8ty_new(Span span);
struct Type i16ty_new(Span span);
struct Type i32ty_new(Span span);
struct Type i64ty_new(Span span);
struct Type isizety_new(Span span);
struct Type boolty_new(Span span);
struct Type unspecified_new(Span span);
struct Type namedty_new(Symbol ident, Span span);
struct Type arrayty_new(struct Type* type, struct Expr* n, Span span);
struct Type tuplety_new(struct TypeSeq tup, Span span);
struct Type recordty_new(struct TypedParamSeq fields, Span span);
struct Type enumty_new(struct EnumVariantSeq variants, Span span);

char const* type_to_string(struct Type const* this);
struct TypeDef {
	Symbol name;
	struct Type type;
	Span span;
};
typedef struct TypeDef TypeDef;

struct TypeDef typedef_new(Symbol name, struct Type type, Span span);
char* typedef_to_string(struct TypeDef const* this);
struct Literal {
	Span span;
	enum {
		IntLit,
		StrLit,
		IdentLit,
		TrueLit,
		FalseLit,
		ThisLit,
	} tag;
	union {
		struct {
			Symbol repr;
		};
	} it;
};
typedef struct Literal Literal;

struct Literal intlit_new(Symbol repr, Span span);
struct Literal strlit_new(Symbol repr, Span span);
struct Literal identlit_new(Symbol repr, Span span);
struct Literal truelit_new(Symbol repr, Span span);
struct Literal falselit_new(Symbol repr, Span span);
struct Literal thislit_new(Symbol repr, Span span);

char const* literal_to_string(struct Literal const* this);
struct StmtSeq {
	struct StmtNode* head;
	Span span;
};
typedef struct StmtSeq StmtSeq;

struct StmtSeq stmtseq_new(struct StmtNode* head, Span span);
void stmtseq_push(struct StmtSeq* seq, struct Stmt el);
struct Stmt* stmtseq_get(struct StmtSeq* seq, usize i);
char const* stmtseq_to_string(struct StmtSeq const* this);
struct StmtNode {
	struct Stmt* val;
	struct StmtNode* next;
};
typedef struct StmtNode StmtNode;

struct StmtNode stmtnode_new(struct Stmt* val);
struct Stmt* stmtnode_get(struct StmtNode* node, usize i);
;
struct Block {
	struct StmtSeq stmts;
	Span span;
};
typedef struct Block Block;

struct Block block_new(struct StmtSeq stmts, Span span);
char* block_to_string(struct Block const* this);
struct ExprSeq {
	struct ExprNode* head;
	Span span;
};
typedef struct ExprSeq ExprSeq;

struct ExprSeq exprseq_new(struct ExprNode* head, Span span);
void exprseq_push(struct ExprSeq* seq, struct Expr el);
struct Expr* exprseq_get(struct ExprSeq* seq, usize i);
char const* exprseq_to_string(struct ExprSeq const* this);
struct ExprNode {
	struct Expr* val;
	struct ExprNode* next;
};
typedef struct ExprNode ExprNode;

struct ExprNode exprnode_new(struct Expr* val);
struct Expr* exprnode_get(struct ExprNode* node, usize i);
;
struct LValue {
	Span span;
	enum {
		This,
		Var,
		MemberAccess,
		Indexing,
		Call,
		PathAccess,
	} tag;
	union {
		struct {
			Symbol v;
		};
		struct {
			struct LValue* rec;
			Symbol member;
		};
		struct {
			struct LValue* arr;
			struct Expr* i;
		};
		struct {
			struct LValue* func;
			struct ExprSeq args;
		};
		struct {
			struct LValue* path;
			Symbol dest;
		};
	} it;
};
typedef struct LValue LValue;

struct LValue this_new(Span span);
struct LValue var_new(Symbol v, Span span);
struct LValue memberaccess_new(struct LValue* rec, Symbol member, Span span);
struct LValue indexing_new(struct LValue* arr, struct Expr* i, Span span);
struct LValue call_new(struct LValue* func, struct ExprSeq args, Span span);
struct LValue pathaccess_new(struct LValue* path, Symbol dest, Span span);

char const* lvalue_to_string(struct LValue const* this);
struct For {
	Symbol var;
	struct Expr* iter;
	struct Block body;
	Span span;
};
typedef struct For For;

struct For for_new(Symbol var, struct Expr* iter, struct Block body, Span span);
char* for_to_string(struct For const* this);
struct Loop {
	struct Block body;
	Span span;
};
typedef struct Loop Loop;

struct Loop loop_new(struct Block body, Span span);
char* loop_to_string(struct Loop const* this);
struct If {
	struct Expr* cond;
	struct Block then;
	struct Block* elsse;
	Span span;
};
typedef struct If If;

struct If if_new(
	struct Expr* cond, struct Block then, struct Block* elsse, Span span
);
char* if_to_string(struct If const* this);
struct Expr {
	Span span;
	enum {
		RetExpr,
		BreakExpr,
		BinOpExpr,
		LitExpr,
		LValueExpr,
		ForExpr,
		LoopExpr,
		IfExpr,
		BlockExpr,
	} tag;
	union {
		struct {
			struct Expr* rval;
		};
		struct {
			struct Expr* bval;
		};
		struct {
			struct Token op;
			struct Expr* left;
			struct Expr* right;
		};
		struct {
			struct Literal lit;
		};
		struct {
			struct LValue lval;
		};
		struct {
			struct For fe;
		};
		struct {
			struct Loop le;
		};
		struct {
			struct If ie;
		};
		struct {
			struct Block b;
		};
	} it;
};
typedef struct Expr Expr;

struct Expr retexpr_new(struct Expr* rval, Span span);
struct Expr breakexpr_new(struct Expr* bval, Span span);
struct Expr binopexpr_new(
	struct Token op, struct Expr* left, struct Expr* right, Span span
);
struct Expr litexpr_new(struct Literal lit, Span span);
struct Expr lvalueexpr_new(struct LValue lval, Span span);
struct Expr forexpr_new(struct For fe, Span span);
struct Expr loopexpr_new(struct Loop le, Span span);
struct Expr ifexpr_new(struct If ie, Span span);
struct Expr blockexpr_new(struct Block b, Span span);

char const* expr_to_string(struct Expr const* this);
struct Assignment {
	struct LValue var;
	struct Expr val;
	Span span;
};
typedef struct Assignment Assignment;

struct Assignment assignment_new(struct LValue var, struct Expr val, Span span);
char* assignment_to_string(struct Assignment const* this);
struct FunctionDef {
	Symbol name;
	struct TypedParamSeq params;
	struct Type* ret;
	struct Block body;
	Span span;
};
typedef struct FunctionDef FunctionDef;

struct FunctionDef functiondef_new(
	Symbol name, struct TypedParamSeq params, struct Type* ret,
	struct Block body, Span span
);
char* functiondef_to_string(struct FunctionDef const* this);
struct FunctionDefSeq {
	struct FunctionDefNode* head;
	Span span;
};
typedef struct FunctionDefSeq FunctionDefSeq;

struct FunctionDefSeq
	functiondefseq_new(struct FunctionDefNode* head, Span span);
void functiondefseq_push(struct FunctionDefSeq* seq, struct FunctionDef el);
struct FunctionDef* functiondefseq_get(struct FunctionDefSeq* seq, usize i);
char const* functiondefseq_to_string(struct FunctionDefSeq const* this);
struct FunctionDefNode {
	struct FunctionDef* val;
	struct FunctionDefNode* next;
};
typedef struct FunctionDefNode FunctionDefNode;

struct FunctionDefNode functiondefnode_new(struct FunctionDef* val);
struct FunctionDef* functiondefnode_get(struct FunctionDefNode* node, usize i);
;
struct TypeImpl {
	Symbol type;
	struct FunctionDefSeq methods;
	Span span;
};
typedef struct TypeImpl TypeImpl;

struct TypeImpl
	typeimpl_new(Symbol type, struct FunctionDefSeq methods, Span span);
char* typeimpl_to_string(struct TypeImpl const* this);
struct VarDefKind {
	Span span;
	enum {
		VarConst,
		VarPersistent,
		VarLocal,
		VarLocalMut,
	} tag;
};
typedef struct VarDefKind VarDefKind;

struct VarDefKind varconst_new(Span span);
struct VarDefKind varpersistent_new(Span span);
struct VarDefKind varlocal_new(Span span);
struct VarDefKind varlocalmut_new(Span span);

char const* vardefkind_to_string(struct VarDefKind const* this);
struct VarDef {
	struct VarDefKind kind;
	struct Expr* val;
	Span span;
};
typedef struct VarDef VarDef;

struct VarDef vardef_new(struct VarDefKind kind, struct Expr* val, Span span);
char* vardef_to_string(struct VarDef const* this);
struct Item {
	Span span;
	enum {
		TypeDefItem,
		FunctionDefItem,
		TypeImplItem,
		VarDefItem,
	} tag;
	union {
		struct {
			struct TypeDef tdef;
		};
		struct {
			struct FunctionDef fdef;
		};
		struct {
			struct TypeImpl timpl;
		};
		struct {
			struct VarDef vdef;
		};
	} it;
};
typedef struct Item Item;

struct Item typedefitem_new(struct TypeDef tdef, Span span);
struct Item functiondefitem_new(struct FunctionDef fdef, Span span);
struct Item typeimplitem_new(struct TypeImpl timpl, Span span);
struct Item vardefitem_new(struct VarDef vdef, Span span);

char const* item_to_string(struct Item const* this);
struct Stmt {
	Span span;
	enum {
		ItemStmt,
		AssignmentStmt,
		ExprStmt,
	} tag;
	union {
		struct {
			struct Item i;
		};
		struct {
			struct Assignment a;
		};
		struct {
			struct Expr e;
		};
	} it;
};
typedef struct Stmt Stmt;

struct Stmt itemstmt_new(struct Item i, Span span);
struct Stmt assignmentstmt_new(struct Assignment a, Span span);
struct Stmt exprstmt_new(struct Expr e, Span span);

char const* stmt_to_string(struct Stmt const* this);
struct ItemSeq {
	struct ItemNode* head;
	Span span;
};
typedef struct ItemSeq ItemSeq;

struct ItemSeq itemseq_new(struct ItemNode* head, Span span);
void itemseq_push(struct ItemSeq* seq, struct Item el);
struct Item* itemseq_get(struct ItemSeq* seq, usize i);
char const* itemseq_to_string(struct ItemSeq const* this);
struct ItemNode {
	struct Item* val;
	struct ItemNode* next;
};
typedef struct ItemNode ItemNode;

struct ItemNode itemnode_new(struct Item* val);
struct Item* itemnode_get(struct ItemNode* node, usize i);
;
struct Program {
	struct ItemSeq items;
	Span span;
};
typedef struct Program Program;

struct Program program_new(struct ItemSeq items, Span span);
char* program_to_string(struct Program const* this);

#endif /* AST_H */