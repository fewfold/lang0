#pragma once

#ifndef DIAGNOSTICS_H
#define DIAGNOSTICS_H

#include "prelude.h"

struct Span {
	usize start;
	usize len;
};
typedef struct Span Span;

Span span_new(usize start, usize len);
Span span_merge(Span a, Span b);
char const* span_to_string(Span const* span);

struct Location {
	usize start_line;
	usize start_col;
	usize end_line;
	usize end_col;
};
typedef struct Location Location;

Location location_new(
	usize start_line, usize start_col, usize end_line, usize end_col
);

struct CompilerContext {
	char const* src;
	char const* path;
};
typedef struct CompilerContext CompilerContext;

Location span_to_location(CompilerContext const* ctx, Span span);

char const** ctx_get_all_lines(CompilerContext const* ctx, Location loc);

enum Severity {
	SeverityError,
	SeverityWarning,
};
typedef enum Severity Severity;

FILE* severity_to_stream(Severity sev);
char const* severity_to_string(Severity sev);

struct Diagnostic {
	CompilerContext const* ctx;
	Severity sev;
	Span span;
	char const* msg;
};
typedef struct Diagnostic Diagnostic;

Diagnostic diagnostic_new(
	CompilerContext const* ctx, Severity sev, Span span, char const* msg
);
void diagnostic_display(Diagnostic dnostic);

#endif /* DIAGNOSTICS_H */
