/********************************************************************
 * This file was generated with the `tools/generate_ast.py` script. *
 * DO NOT MODIFY!                                                   *
 ********************************************************************/

#include "ast.h"

extern usize strlen(char const*);
extern char* strncat(char*, char const*, usize);

static inline void strextend(char** dest, usize* dlen, char const* src) {
	usize slen = strlen(src);
	*dlen += slen;
	*dest = (char*) realloc(
		*dest, (*dlen + 1 < 32 ? 32 : *dlen + 1) * sizeof(char)
	);
	*dest = strncat(*dest, src, slen);
}

struct Token token_new(Span span, Symbol repr) {
	struct Token it = {
		.span = span,
		.repr = repr,
	};
	return it;
}

char* token_to_string(struct Token const* this) {
	usize total_len = 0;
	char* res = (char*) malloc(sizeof(char));
	res[0] = '\0';
	strextend(&res, &total_len, "(Token ");

	if (&this->repr) {
		char const* field = symbol_to_string(&this->repr);
		strextend(&res, &total_len, field);
		strextend(&res, &total_len, " ");
	}
	res[total_len - 1] = ')';
	res[total_len] = '\0';

	return res;
}

struct TypedParam typedparam_new(Symbol name, struct Type* type, Span span) {
	struct TypedParam it = {
		.name = name,
		.type = type,
		.span = span,
	};
	return it;
}

char* typedparam_to_string(struct TypedParam const* this) {
	usize total_len = 0;
	char* res = (char*) malloc(sizeof(char));
	res[0] = '\0';
	strextend(&res, &total_len, "(TypedParam ");

	if (&this->name) {
		char const* field = symbol_to_string(&this->name);
		strextend(&res, &total_len, field);
		strextend(&res, &total_len, " ");
	}

	if (this->type) {
		char const* field = type_to_string(this->type);
		strextend(&res, &total_len, field);
		strextend(&res, &total_len, " ");
	}
	res[total_len - 1] = ')';
	res[total_len] = '\0';

	return res;
}

struct TypeSeq typeseq_new(struct TypeNode* head, Span span) {
	struct TypeSeq it = {
		.head = head,
		.span = span,
	};
	return it;
}

void typeseq_push(struct TypeSeq* seq, struct Type el) {
	struct Type* pel = (struct Type*) malloc(sizeof(struct Type));
	*pel = el;
	struct TypeNode new_node = typenode_new(pel);
	new_node.next = seq->head;
	struct TypeNode* pnode = (struct TypeNode*) malloc(sizeof(struct TypeNode));
	*pnode = new_node;
	seq->head = pnode;
}

struct Type* typeseq_get(struct TypeSeq* seq, usize i) {
	return typenode_get(seq->head, i);
}

char const* typeseq_to_string(struct TypeSeq const* this) {
	usize total_len = 0;
	char* res = (char*) malloc(sizeof(char));
	res[0] = '\0';

	for (TypeNode* curr = this->head; curr != NULL; curr = curr->next) {
		char const* el = type_to_string(curr->val);
		strextend(&res, &total_len, el);
		strextend(&res, &total_len, " ");
	}

	if (this->head) {
		res[total_len - 1] = ')';
		res[total_len] = '\0';
	}

	return res;
}

struct TypeNode typenode_new(struct Type* val) {
	struct TypeNode it = {
		.val = val,
		.next = NULL,
	};
	return it;
}

struct Type* typenode_get(struct TypeNode* node, usize i) {
	if (i == 0) {
		return node->val;
	} else {
		assertm(node->next != NULL, "index out of bounds!");
		return typenode_get(node->next, i - 1);
	}
}

struct TypedParamSeq typedparamseq_new(struct TypedParamNode* head, Span span) {
	struct TypedParamSeq it = {
		.head = head,
		.span = span,
	};
	return it;
}

void typedparamseq_push(struct TypedParamSeq* seq, struct TypedParam el) {
	struct TypedParam* pel
		= (struct TypedParam*) malloc(sizeof(struct TypedParam));
	*pel = el;
	struct TypedParamNode new_node = typedparamnode_new(pel);
	new_node.next = seq->head;
	struct TypedParamNode* pnode
		= (struct TypedParamNode*) malloc(sizeof(struct TypedParamNode));
	*pnode = new_node;
	seq->head = pnode;
}

struct TypedParam* typedparamseq_get(struct TypedParamSeq* seq, usize i) {
	return typedparamnode_get(seq->head, i);
}

char const* typedparamseq_to_string(struct TypedParamSeq const* this) {
	usize total_len = 0;
	char* res = (char*) malloc(sizeof(char));
	res[0] = '\0';

	for (TypedParamNode* curr = this->head; curr != NULL; curr = curr->next) {
		char const* el = typedparam_to_string(curr->val);
		strextend(&res, &total_len, el);
		strextend(&res, &total_len, " ");
	}

	if (this->head) {
		res[total_len - 1] = ')';
		res[total_len] = '\0';
	}

	return res;
}

struct TypedParamNode typedparamnode_new(struct TypedParam* val) {
	struct TypedParamNode it = {
		.val = val,
		.next = NULL,
	};
	return it;
}

struct TypedParam* typedparamnode_get(struct TypedParamNode* node, usize i) {
	if (i == 0) {
		return node->val;
	} else {
		assertm(node->next != NULL, "index out of bounds!");
		return typedparamnode_get(node->next, i - 1);
	}
}

struct EnumVariant enumvariant_new(Symbol name, struct TypeSeq tup, Span span) {
	struct EnumVariant it = {
		.name = name,
		.tup = tup,
		.span = span,
	};
	return it;
}

char* enumvariant_to_string(struct EnumVariant const* this) {
	usize total_len = 0;
	char* res = (char*) malloc(sizeof(char));
	res[0] = '\0';
	strextend(&res, &total_len, "(EnumVariant ");

	if (&this->name) {
		char const* field = symbol_to_string(&this->name);
		strextend(&res, &total_len, field);
		strextend(&res, &total_len, " ");
	}

	if (&this->tup) {
		char const* field = typeseq_to_string(&this->tup);
		strextend(&res, &total_len, field);
		strextend(&res, &total_len, " ");
	}
	res[total_len - 1] = ')';
	res[total_len] = '\0';

	return res;
}

struct EnumVariantSeq
	enumvariantseq_new(struct EnumVariantNode* head, Span span) {
	struct EnumVariantSeq it = {
		.head = head,
		.span = span,
	};
	return it;
}

void enumvariantseq_push(struct EnumVariantSeq* seq, struct EnumVariant el) {
	struct EnumVariant* pel
		= (struct EnumVariant*) malloc(sizeof(struct EnumVariant));
	*pel = el;
	struct EnumVariantNode new_node = enumvariantnode_new(pel);
	new_node.next = seq->head;
	struct EnumVariantNode* pnode
		= (struct EnumVariantNode*) malloc(sizeof(struct EnumVariantNode));
	*pnode = new_node;
	seq->head = pnode;
}

struct EnumVariant* enumvariantseq_get(struct EnumVariantSeq* seq, usize i) {
	return enumvariantnode_get(seq->head, i);
}

char const* enumvariantseq_to_string(struct EnumVariantSeq const* this) {
	usize total_len = 0;
	char* res = (char*) malloc(sizeof(char));
	res[0] = '\0';

	for (EnumVariantNode* curr = this->head; curr != NULL; curr = curr->next) {
		char const* el = enumvariant_to_string(curr->val);
		strextend(&res, &total_len, el);
		strextend(&res, &total_len, " ");
	}

	if (this->head) {
		res[total_len - 1] = ')';
		res[total_len] = '\0';
	}

	return res;
}

struct EnumVariantNode enumvariantnode_new(struct EnumVariant* val) {
	struct EnumVariantNode it = {
		.val = val,
		.next = NULL,
	};
	return it;
}

struct EnumVariant* enumvariantnode_get(struct EnumVariantNode* node, usize i) {
	if (i == 0) {
		return node->val;
	} else {
		assertm(node->next != NULL, "index out of bounds!");
		return enumvariantnode_get(node->next, i - 1);
	}
}

struct Type charty_new(Span span) {
	struct Type it = { .tag = CharTy };
	it.span = span;
	return it;
}

struct Type f32ty_new(Span span) {
	struct Type it = { .tag = F32Ty };
	it.span = span;
	return it;
}

struct Type f64ty_new(Span span) {
	struct Type it = { .tag = F64Ty };
	it.span = span;
	return it;
}

struct Type u8ty_new(Span span) {
	struct Type it = { .tag = U8Ty };
	it.span = span;
	return it;
}

struct Type u16ty_new(Span span) {
	struct Type it = { .tag = U16Ty };
	it.span = span;
	return it;
}

struct Type u32ty_new(Span span) {
	struct Type it = { .tag = U32Ty };
	it.span = span;
	return it;
}

struct Type u64ty_new(Span span) {
	struct Type it = { .tag = U64Ty };
	it.span = span;
	return it;
}

struct Type usizety_new(Span span) {
	struct Type it = { .tag = UsizeTy };
	it.span = span;
	return it;
}

struct Type i8ty_new(Span span) {
	struct Type it = { .tag = I8Ty };
	it.span = span;
	return it;
}

struct Type i16ty_new(Span span) {
	struct Type it = { .tag = I16Ty };
	it.span = span;
	return it;
}

struct Type i32ty_new(Span span) {
	struct Type it = { .tag = I32Ty };
	it.span = span;
	return it;
}

struct Type i64ty_new(Span span) {
	struct Type it = { .tag = I64Ty };
	it.span = span;
	return it;
}

struct Type isizety_new(Span span) {
	struct Type it = { .tag = ISizeTy };
	it.span = span;
	return it;
}

struct Type boolty_new(Span span) {
	struct Type it = { .tag = BoolTy };
	it.span = span;
	return it;
}

struct Type unspecified_new(Span span) {
	struct Type it = { .tag = Unspecified };
	it.span = span;
	return it;
}

struct Type namedty_new(Symbol ident, Span span) {
	struct Type it = { .tag = NamedTy };
	it.it.ident = ident;
	it.span = span;
	return it;
}

struct Type arrayty_new(struct Type* type, struct Expr* n, Span span) {
	struct Type it = { .tag = ArrayTy };
	it.it.type = type;
	it.it.n = n;
	it.span = span;
	return it;
}

struct Type tuplety_new(struct TypeSeq tup, Span span) {
	struct Type it = { .tag = TupleTy };
	it.it.tup = tup;
	it.span = span;
	return it;
}

struct Type recordty_new(struct TypedParamSeq fields, Span span) {
	struct Type it = { .tag = RecordTy };
	it.it.fields = fields;
	it.span = span;
	return it;
}

struct Type enumty_new(struct EnumVariantSeq variants, Span span) {
	struct Type it = { .tag = EnumTy };
	it.it.variants = variants;
	it.span = span;
	return it;
}

char const* type_to_string(struct Type const* this) {
	usize total_len = 0;
	char* res = (char*) malloc(sizeof(char));
	res[0] = '\0';
	strextend(&res, &total_len, "(");
	char const* field = NULL;
	switch (this->tag) {
		case CharTy: strextend(&res, &total_len, "Type::CharTy "); break;
		case F32Ty: strextend(&res, &total_len, "Type::F32Ty "); break;
		case F64Ty: strextend(&res, &total_len, "Type::F64Ty "); break;
		case U8Ty: strextend(&res, &total_len, "Type::U8Ty "); break;
		case U16Ty: strextend(&res, &total_len, "Type::U16Ty "); break;
		case U32Ty: strextend(&res, &total_len, "Type::U32Ty "); break;
		case U64Ty: strextend(&res, &total_len, "Type::U64Ty "); break;
		case UsizeTy: strextend(&res, &total_len, "Type::UsizeTy "); break;
		case I8Ty: strextend(&res, &total_len, "Type::I8Ty "); break;
		case I16Ty: strextend(&res, &total_len, "Type::I16Ty "); break;
		case I32Ty: strextend(&res, &total_len, "Type::I32Ty "); break;
		case I64Ty: strextend(&res, &total_len, "Type::I64Ty "); break;
		case ISizeTy: strextend(&res, &total_len, "Type::ISizeTy "); break;
		case BoolTy: strextend(&res, &total_len, "Type::BoolTy "); break;
		case Unspecified:
			strextend(&res, &total_len, "Type::Unspecified ");
			break;
		case NamedTy:
			strextend(&res, &total_len, "Type::NamedTy ");
			field = symbol_to_string(&this->it.ident);
			strextend(&res, &total_len, field);
			strextend(&res, &total_len, " ");
			break;
		case ArrayTy:
			strextend(&res, &total_len, "Type::ArrayTy ");
			field = type_to_string(this->it.type);
			strextend(&res, &total_len, field);
			strextend(&res, &total_len, " ");
			field = expr_to_string(this->it.n);
			strextend(&res, &total_len, field);
			strextend(&res, &total_len, " ");
			break;
		case TupleTy:
			strextend(&res, &total_len, "Type::TupleTy ");
			field = typeseq_to_string(&this->it.tup);
			strextend(&res, &total_len, field);
			strextend(&res, &total_len, " ");
			break;
		case RecordTy:
			strextend(&res, &total_len, "Type::RecordTy ");
			field = typedparamseq_to_string(&this->it.fields);
			strextend(&res, &total_len, field);
			strextend(&res, &total_len, " ");
			break;
		case EnumTy:
			strextend(&res, &total_len, "Type::EnumTy ");
			field = enumvariantseq_to_string(&this->it.variants);
			strextend(&res, &total_len, field);
			strextend(&res, &total_len, " ");
			break;
	}
	res[total_len - 1] = ')';
	res[total_len] = '\0';
	(void) field; // if enum has no variant

	return res;
}

struct TypeDef typedef_new(Symbol name, struct Type type, Span span) {
	struct TypeDef it = {
		.name = name,
		.type = type,
		.span = span,
	};
	return it;
}

char* typedef_to_string(struct TypeDef const* this) {
	usize total_len = 0;
	char* res = (char*) malloc(sizeof(char));
	res[0] = '\0';
	strextend(&res, &total_len, "(TypeDef ");

	if (&this->name) {
		char const* field = symbol_to_string(&this->name);
		strextend(&res, &total_len, field);
		strextend(&res, &total_len, " ");
	}

	if (&this->type) {
		char const* field = type_to_string(&this->type);
		strextend(&res, &total_len, field);
		strextend(&res, &total_len, " ");
	}
	res[total_len - 1] = ')';
	res[total_len] = '\0';

	return res;
}

struct Literal intlit_new(Symbol repr, Span span) {
	struct Literal it = { .tag = IntLit };
	it.it.repr = repr;
	it.span = span;
	return it;
}

struct Literal strlit_new(Symbol repr, Span span) {
	struct Literal it = { .tag = StrLit };
	it.it.repr = repr;
	it.span = span;
	return it;
}

struct Literal identlit_new(Symbol repr, Span span) {
	struct Literal it = { .tag = IdentLit };
	it.it.repr = repr;
	it.span = span;
	return it;
}

struct Literal truelit_new(Symbol repr, Span span) {
	struct Literal it = { .tag = TrueLit };
	it.it.repr = repr;
	it.span = span;
	return it;
}

struct Literal falselit_new(Symbol repr, Span span) {
	struct Literal it = { .tag = FalseLit };
	it.it.repr = repr;
	it.span = span;
	return it;
}

struct Literal thislit_new(Symbol repr, Span span) {
	struct Literal it = { .tag = ThisLit };
	it.it.repr = repr;
	it.span = span;
	return it;
}

char const* literal_to_string(struct Literal const* this) {
	usize total_len = 0;
	char* res = (char*) malloc(sizeof(char));
	res[0] = '\0';
	strextend(&res, &total_len, "(");
	char const* field = NULL;
	switch (this->tag) {
		case IntLit:
			strextend(&res, &total_len, "Literal::IntLit ");
			field = symbol_to_string(&this->it.repr);
			strextend(&res, &total_len, field);
			strextend(&res, &total_len, " ");
			break;
		case StrLit:
			strextend(&res, &total_len, "Literal::StrLit ");
			field = symbol_to_string(&this->it.repr);
			strextend(&res, &total_len, field);
			strextend(&res, &total_len, " ");
			break;
		case IdentLit:
			strextend(&res, &total_len, "Literal::IdentLit ");
			field = symbol_to_string(&this->it.repr);
			strextend(&res, &total_len, field);
			strextend(&res, &total_len, " ");
			break;
		case TrueLit:
			strextend(&res, &total_len, "Literal::TrueLit ");
			field = symbol_to_string(&this->it.repr);
			strextend(&res, &total_len, field);
			strextend(&res, &total_len, " ");
			break;
		case FalseLit:
			strextend(&res, &total_len, "Literal::FalseLit ");
			field = symbol_to_string(&this->it.repr);
			strextend(&res, &total_len, field);
			strextend(&res, &total_len, " ");
			break;
		case ThisLit:
			strextend(&res, &total_len, "Literal::ThisLit ");
			field = symbol_to_string(&this->it.repr);
			strextend(&res, &total_len, field);
			strextend(&res, &total_len, " ");
			break;
	}
	res[total_len - 1] = ')';
	res[total_len] = '\0';
	(void) field; // if enum has no variant

	return res;
}

struct StmtSeq stmtseq_new(struct StmtNode* head, Span span) {
	struct StmtSeq it = {
		.head = head,
		.span = span,
	};
	return it;
}

void stmtseq_push(struct StmtSeq* seq, struct Stmt el) {
	struct Stmt* pel = (struct Stmt*) malloc(sizeof(struct Stmt));
	*pel = el;
	struct StmtNode new_node = stmtnode_new(pel);
	new_node.next = seq->head;
	struct StmtNode* pnode = (struct StmtNode*) malloc(sizeof(struct StmtNode));
	*pnode = new_node;
	seq->head = pnode;
}

struct Stmt* stmtseq_get(struct StmtSeq* seq, usize i) {
	return stmtnode_get(seq->head, i);
}

char const* stmtseq_to_string(struct StmtSeq const* this) {
	usize total_len = 0;
	char* res = (char*) malloc(sizeof(char));
	res[0] = '\0';

	for (StmtNode* curr = this->head; curr != NULL; curr = curr->next) {
		char const* el = stmt_to_string(curr->val);
		strextend(&res, &total_len, el);
		strextend(&res, &total_len, " ");
	}

	if (this->head) {
		res[total_len - 1] = ')';
		res[total_len] = '\0';
	}

	return res;
}

struct StmtNode stmtnode_new(struct Stmt* val) {
	struct StmtNode it = {
		.val = val,
		.next = NULL,
	};
	return it;
}

struct Stmt* stmtnode_get(struct StmtNode* node, usize i) {
	if (i == 0) {
		return node->val;
	} else {
		assertm(node->next != NULL, "index out of bounds!");
		return stmtnode_get(node->next, i - 1);
	}
}

struct Block block_new(struct StmtSeq stmts, Span span) {
	struct Block it = {
		.stmts = stmts,
		.span = span,
	};
	return it;
}

char* block_to_string(struct Block const* this) {
	usize total_len = 0;
	char* res = (char*) malloc(sizeof(char));
	res[0] = '\0';
	strextend(&res, &total_len, "(Block ");

	if (&this->stmts) {
		char const* field = stmtseq_to_string(&this->stmts);
		strextend(&res, &total_len, field);
		strextend(&res, &total_len, " ");
	}
	res[total_len - 1] = ')';
	res[total_len] = '\0';

	return res;
}

struct ExprSeq exprseq_new(struct ExprNode* head, Span span) {
	struct ExprSeq it = {
		.head = head,
		.span = span,
	};
	return it;
}

void exprseq_push(struct ExprSeq* seq, struct Expr el) {
	struct Expr* pel = (struct Expr*) malloc(sizeof(struct Expr));
	*pel = el;
	struct ExprNode new_node = exprnode_new(pel);
	new_node.next = seq->head;
	struct ExprNode* pnode = (struct ExprNode*) malloc(sizeof(struct ExprNode));
	*pnode = new_node;
	seq->head = pnode;
}

struct Expr* exprseq_get(struct ExprSeq* seq, usize i) {
	return exprnode_get(seq->head, i);
}

char const* exprseq_to_string(struct ExprSeq const* this) {
	usize total_len = 0;
	char* res = (char*) malloc(sizeof(char));
	res[0] = '\0';

	for (ExprNode* curr = this->head; curr != NULL; curr = curr->next) {
		char const* el = expr_to_string(curr->val);
		strextend(&res, &total_len, el);
		strextend(&res, &total_len, " ");
	}

	if (this->head) {
		res[total_len - 1] = ')';
		res[total_len] = '\0';
	}

	return res;
}

struct ExprNode exprnode_new(struct Expr* val) {
	struct ExprNode it = {
		.val = val,
		.next = NULL,
	};
	return it;
}

struct Expr* exprnode_get(struct ExprNode* node, usize i) {
	if (i == 0) {
		return node->val;
	} else {
		assertm(node->next != NULL, "index out of bounds!");
		return exprnode_get(node->next, i - 1);
	}
}

struct LValue this_new(Span span) {
	struct LValue it = { .tag = This };
	it.span = span;
	return it;
}

struct LValue var_new(Symbol v, Span span) {
	struct LValue it = { .tag = Var };
	it.it.v = v;
	it.span = span;
	return it;
}

struct LValue memberaccess_new(struct LValue* rec, Symbol member, Span span) {
	struct LValue it = { .tag = MemberAccess };
	it.it.rec = rec;
	it.it.member = member;
	it.span = span;
	return it;
}

struct LValue indexing_new(struct LValue* arr, struct Expr* i, Span span) {
	struct LValue it = { .tag = Indexing };
	it.it.arr = arr;
	it.it.i = i;
	it.span = span;
	return it;
}

struct LValue call_new(struct LValue* func, struct ExprSeq args, Span span) {
	struct LValue it = { .tag = Call };
	it.it.func = func;
	it.it.args = args;
	it.span = span;
	return it;
}

struct LValue pathaccess_new(struct LValue* path, Symbol dest, Span span) {
	struct LValue it = { .tag = PathAccess };
	it.it.path = path;
	it.it.dest = dest;
	it.span = span;
	return it;
}

char const* lvalue_to_string(struct LValue const* this) {
	usize total_len = 0;
	char* res = (char*) malloc(sizeof(char));
	res[0] = '\0';
	strextend(&res, &total_len, "(");
	char const* field = NULL;
	switch (this->tag) {
		case This: strextend(&res, &total_len, "LValue::This "); break;
		case Var:
			strextend(&res, &total_len, "LValue::Var ");
			field = symbol_to_string(&this->it.v);
			strextend(&res, &total_len, field);
			strextend(&res, &total_len, " ");
			break;
		case MemberAccess:
			strextend(&res, &total_len, "LValue::MemberAccess ");
			field = lvalue_to_string(this->it.rec);
			strextend(&res, &total_len, field);
			strextend(&res, &total_len, " ");
			field = symbol_to_string(&this->it.member);
			strextend(&res, &total_len, field);
			strextend(&res, &total_len, " ");
			break;
		case Indexing:
			strextend(&res, &total_len, "LValue::Indexing ");
			field = lvalue_to_string(this->it.arr);
			strextend(&res, &total_len, field);
			strextend(&res, &total_len, " ");
			field = expr_to_string(this->it.i);
			strextend(&res, &total_len, field);
			strextend(&res, &total_len, " ");
			break;
		case Call:
			strextend(&res, &total_len, "LValue::Call ");
			field = lvalue_to_string(this->it.func);
			strextend(&res, &total_len, field);
			strextend(&res, &total_len, " ");
			field = exprseq_to_string(&this->it.args);
			strextend(&res, &total_len, field);
			strextend(&res, &total_len, " ");
			break;
		case PathAccess:
			strextend(&res, &total_len, "LValue::PathAccess ");
			field = lvalue_to_string(this->it.path);
			strextend(&res, &total_len, field);
			strextend(&res, &total_len, " ");
			field = symbol_to_string(&this->it.dest);
			strextend(&res, &total_len, field);
			strextend(&res, &total_len, " ");
			break;
	}
	res[total_len - 1] = ')';
	res[total_len] = '\0';
	(void) field; // if enum has no variant

	return res;
}

struct For
	for_new(Symbol var, struct Expr* iter, struct Block body, Span span) {
	struct For it = {
		.var = var,
		.iter = iter,
		.body = body,
		.span = span,
	};
	return it;
}

char* for_to_string(struct For const* this) {
	usize total_len = 0;
	char* res = (char*) malloc(sizeof(char));
	res[0] = '\0';
	strextend(&res, &total_len, "(For ");

	if (&this->var) {
		char const* field = symbol_to_string(&this->var);
		strextend(&res, &total_len, field);
		strextend(&res, &total_len, " ");
	}

	if (this->iter) {
		char const* field = expr_to_string(this->iter);
		strextend(&res, &total_len, field);
		strextend(&res, &total_len, " ");
	}

	if (&this->body) {
		char const* field = block_to_string(&this->body);
		strextend(&res, &total_len, field);
		strextend(&res, &total_len, " ");
	}
	res[total_len - 1] = ')';
	res[total_len] = '\0';

	return res;
}

struct Loop loop_new(struct Block body, Span span) {
	struct Loop it = {
		.body = body,
		.span = span,
	};
	return it;
}

char* loop_to_string(struct Loop const* this) {
	usize total_len = 0;
	char* res = (char*) malloc(sizeof(char));
	res[0] = '\0';
	strextend(&res, &total_len, "(Loop ");

	if (&this->body) {
		char const* field = block_to_string(&this->body);
		strextend(&res, &total_len, field);
		strextend(&res, &total_len, " ");
	}
	res[total_len - 1] = ')';
	res[total_len] = '\0';

	return res;
}

struct If if_new(
	struct Expr* cond, struct Block then, struct Block* elsse, Span span
) {
	struct If it = {
		.cond = cond,
		.then = then,
		.elsse = elsse,
		.span = span,
	};
	return it;
}

char* if_to_string(struct If const* this) {
	usize total_len = 0;
	char* res = (char*) malloc(sizeof(char));
	res[0] = '\0';
	strextend(&res, &total_len, "(If ");

	if (this->cond) {
		char const* field = expr_to_string(this->cond);
		strextend(&res, &total_len, field);
		strextend(&res, &total_len, " ");
	}

	if (&this->then) {
		char const* field = block_to_string(&this->then);
		strextend(&res, &total_len, field);
		strextend(&res, &total_len, " ");
	}

	if (this->elsse) {
		char const* field = block_to_string(this->elsse);
		strextend(&res, &total_len, field);
		strextend(&res, &total_len, " ");
	}
	res[total_len - 1] = ')';
	res[total_len] = '\0';

	return res;
}

struct Expr retexpr_new(struct Expr* rval, Span span) {
	struct Expr it = { .tag = RetExpr };
	it.it.rval = rval;
	it.span = span;
	return it;
}

struct Expr breakexpr_new(struct Expr* bval, Span span) {
	struct Expr it = { .tag = BreakExpr };
	it.it.bval = bval;
	it.span = span;
	return it;
}

struct Expr binopexpr_new(
	struct Token op, struct Expr* left, struct Expr* right, Span span
) {
	struct Expr it = { .tag = BinOpExpr };
	it.it.op = op;
	it.it.left = left;
	it.it.right = right;
	it.span = span;
	return it;
}

struct Expr litexpr_new(struct Literal lit, Span span) {
	struct Expr it = { .tag = LitExpr };
	it.it.lit = lit;
	it.span = span;
	return it;
}

struct Expr lvalueexpr_new(struct LValue lval, Span span) {
	struct Expr it = { .tag = LValueExpr };
	it.it.lval = lval;
	it.span = span;
	return it;
}

struct Expr forexpr_new(struct For fe, Span span) {
	struct Expr it = { .tag = ForExpr };
	it.it.fe = fe;
	it.span = span;
	return it;
}

struct Expr loopexpr_new(struct Loop le, Span span) {
	struct Expr it = { .tag = LoopExpr };
	it.it.le = le;
	it.span = span;
	return it;
}

struct Expr ifexpr_new(struct If ie, Span span) {
	struct Expr it = { .tag = IfExpr };
	it.it.ie = ie;
	it.span = span;
	return it;
}

struct Expr blockexpr_new(struct Block b, Span span) {
	struct Expr it = { .tag = BlockExpr };
	it.it.b = b;
	it.span = span;
	return it;
}

char const* expr_to_string(struct Expr const* this) {
	usize total_len = 0;
	char* res = (char*) malloc(sizeof(char));
	res[0] = '\0';
	strextend(&res, &total_len, "(");
	char const* field = NULL;
	switch (this->tag) {
		case RetExpr:
			strextend(&res, &total_len, "Expr::RetExpr ");
			field = expr_to_string(this->it.rval);
			strextend(&res, &total_len, field);
			strextend(&res, &total_len, " ");
			break;
		case BreakExpr:
			strextend(&res, &total_len, "Expr::BreakExpr ");
			field = expr_to_string(this->it.bval);
			strextend(&res, &total_len, field);
			strextend(&res, &total_len, " ");
			break;
		case BinOpExpr:
			strextend(&res, &total_len, "Expr::BinOpExpr ");
			field = token_to_string(&this->it.op);
			strextend(&res, &total_len, field);
			strextend(&res, &total_len, " ");
			field = expr_to_string(this->it.left);
			strextend(&res, &total_len, field);
			strextend(&res, &total_len, " ");
			field = expr_to_string(this->it.right);
			strextend(&res, &total_len, field);
			strextend(&res, &total_len, " ");
			break;
		case LitExpr:
			strextend(&res, &total_len, "Expr::LitExpr ");
			field = literal_to_string(&this->it.lit);
			strextend(&res, &total_len, field);
			strextend(&res, &total_len, " ");
			break;
		case LValueExpr:
			strextend(&res, &total_len, "Expr::LValueExpr ");
			field = lvalue_to_string(&this->it.lval);
			strextend(&res, &total_len, field);
			strextend(&res, &total_len, " ");
			break;
		case ForExpr:
			strextend(&res, &total_len, "Expr::ForExpr ");
			field = for_to_string(&this->it.fe);
			strextend(&res, &total_len, field);
			strextend(&res, &total_len, " ");
			break;
		case LoopExpr:
			strextend(&res, &total_len, "Expr::LoopExpr ");
			field = loop_to_string(&this->it.le);
			strextend(&res, &total_len, field);
			strextend(&res, &total_len, " ");
			break;
		case IfExpr:
			strextend(&res, &total_len, "Expr::IfExpr ");
			field = if_to_string(&this->it.ie);
			strextend(&res, &total_len, field);
			strextend(&res, &total_len, " ");
			break;
		case BlockExpr:
			strextend(&res, &total_len, "Expr::BlockExpr ");
			field = block_to_string(&this->it.b);
			strextend(&res, &total_len, field);
			strextend(&res, &total_len, " ");
			break;
	}
	res[total_len - 1] = ')';
	res[total_len] = '\0';
	(void) field; // if enum has no variant

	return res;
}

struct Assignment
	assignment_new(struct LValue var, struct Expr val, Span span) {
	struct Assignment it = {
		.var = var,
		.val = val,
		.span = span,
	};
	return it;
}

char* assignment_to_string(struct Assignment const* this) {
	usize total_len = 0;
	char* res = (char*) malloc(sizeof(char));
	res[0] = '\0';
	strextend(&res, &total_len, "(Assignment ");

	if (&this->var) {
		char const* field = lvalue_to_string(&this->var);
		strextend(&res, &total_len, field);
		strextend(&res, &total_len, " ");
	}

	if (&this->val) {
		char const* field = expr_to_string(&this->val);
		strextend(&res, &total_len, field);
		strextend(&res, &total_len, " ");
	}
	res[total_len - 1] = ')';
	res[total_len] = '\0';

	return res;
}

struct FunctionDef functiondef_new(
	Symbol name, struct TypedParamSeq params, struct Type* ret,
	struct Block body, Span span
) {
	struct FunctionDef it = {
		.name = name,
		.params = params,
		.ret = ret,
		.body = body,
		.span = span,
	};
	return it;
}

char* functiondef_to_string(struct FunctionDef const* this) {
	usize total_len = 0;
	char* res = (char*) malloc(sizeof(char));
	res[0] = '\0';
	strextend(&res, &total_len, "(FunctionDef ");

	if (&this->name) {
		char const* field = symbol_to_string(&this->name);
		strextend(&res, &total_len, field);
		strextend(&res, &total_len, " ");
	}

	if (&this->params) {
		char const* field = typedparamseq_to_string(&this->params);
		strextend(&res, &total_len, field);
		strextend(&res, &total_len, " ");
	}

	if (this->ret) {
		char const* field = type_to_string(this->ret);
		strextend(&res, &total_len, field);
		strextend(&res, &total_len, " ");
	}

	if (&this->body) {
		char const* field = block_to_string(&this->body);
		strextend(&res, &total_len, field);
		strextend(&res, &total_len, " ");
	}
	res[total_len - 1] = ')';
	res[total_len] = '\0';

	return res;
}

struct FunctionDefSeq
	functiondefseq_new(struct FunctionDefNode* head, Span span) {
	struct FunctionDefSeq it = {
		.head = head,
		.span = span,
	};
	return it;
}

void functiondefseq_push(struct FunctionDefSeq* seq, struct FunctionDef el) {
	struct FunctionDef* pel
		= (struct FunctionDef*) malloc(sizeof(struct FunctionDef));
	*pel = el;
	struct FunctionDefNode new_node = functiondefnode_new(pel);
	new_node.next = seq->head;
	struct FunctionDefNode* pnode
		= (struct FunctionDefNode*) malloc(sizeof(struct FunctionDefNode));
	*pnode = new_node;
	seq->head = pnode;
}

struct FunctionDef* functiondefseq_get(struct FunctionDefSeq* seq, usize i) {
	return functiondefnode_get(seq->head, i);
}

char const* functiondefseq_to_string(struct FunctionDefSeq const* this) {
	usize total_len = 0;
	char* res = (char*) malloc(sizeof(char));
	res[0] = '\0';

	for (FunctionDefNode* curr = this->head; curr != NULL; curr = curr->next) {
		char const* el = functiondef_to_string(curr->val);
		strextend(&res, &total_len, el);
		strextend(&res, &total_len, " ");
	}

	if (this->head) {
		res[total_len - 1] = ')';
		res[total_len] = '\0';
	}

	return res;
}

struct FunctionDefNode functiondefnode_new(struct FunctionDef* val) {
	struct FunctionDefNode it = {
		.val = val,
		.next = NULL,
	};
	return it;
}

struct FunctionDef* functiondefnode_get(struct FunctionDefNode* node, usize i) {
	if (i == 0) {
		return node->val;
	} else {
		assertm(node->next != NULL, "index out of bounds!");
		return functiondefnode_get(node->next, i - 1);
	}
}

struct TypeImpl
	typeimpl_new(Symbol type, struct FunctionDefSeq methods, Span span) {
	struct TypeImpl it = {
		.type = type,
		.methods = methods,
		.span = span,
	};
	return it;
}

char* typeimpl_to_string(struct TypeImpl const* this) {
	usize total_len = 0;
	char* res = (char*) malloc(sizeof(char));
	res[0] = '\0';
	strextend(&res, &total_len, "(TypeImpl ");

	if (&this->type) {
		char const* field = symbol_to_string(&this->type);
		strextend(&res, &total_len, field);
		strextend(&res, &total_len, " ");
	}

	if (&this->methods) {
		char const* field = functiondefseq_to_string(&this->methods);
		strextend(&res, &total_len, field);
		strextend(&res, &total_len, " ");
	}
	res[total_len - 1] = ')';
	res[total_len] = '\0';

	return res;
}

struct VarDefKind varconst_new(Span span) {
	struct VarDefKind it = { .tag = VarConst };
	it.span = span;
	return it;
}

struct VarDefKind varpersistent_new(Span span) {
	struct VarDefKind it = { .tag = VarPersistent };
	it.span = span;
	return it;
}

struct VarDefKind varlocal_new(Span span) {
	struct VarDefKind it = { .tag = VarLocal };
	it.span = span;
	return it;
}

struct VarDefKind varlocalmut_new(Span span) {
	struct VarDefKind it = { .tag = VarLocalMut };
	it.span = span;
	return it;
}

char const* vardefkind_to_string(struct VarDefKind const* this) {
	usize total_len = 0;
	char* res = (char*) malloc(sizeof(char));
	res[0] = '\0';
	strextend(&res, &total_len, "(");
	char const* field = NULL;
	switch (this->tag) {
		case VarConst:
			strextend(&res, &total_len, "VarDefKind::VarConst ");
			break;
		case VarPersistent:
			strextend(&res, &total_len, "VarDefKind::VarPersistent ");
			break;
		case VarLocal:
			strextend(&res, &total_len, "VarDefKind::VarLocal ");
			break;
		case VarLocalMut:
			strextend(&res, &total_len, "VarDefKind::VarLocalMut ");
			break;
	}
	res[total_len - 1] = ')';
	res[total_len] = '\0';
	(void) field; // if enum has no variant

	return res;
}

struct VarDef vardef_new(struct VarDefKind kind, struct Expr* val, Span span) {
	struct VarDef it = {
		.kind = kind,
		.val = val,
		.span = span,
	};
	return it;
}

char* vardef_to_string(struct VarDef const* this) {
	usize total_len = 0;
	char* res = (char*) malloc(sizeof(char));
	res[0] = '\0';
	strextend(&res, &total_len, "(VarDef ");

	if (&this->kind) {
		char const* field = vardefkind_to_string(&this->kind);
		strextend(&res, &total_len, field);
		strextend(&res, &total_len, " ");
	}

	if (this->val) {
		char const* field = expr_to_string(this->val);
		strextend(&res, &total_len, field);
		strextend(&res, &total_len, " ");
	}
	res[total_len - 1] = ')';
	res[total_len] = '\0';

	return res;
}

struct Item typedefitem_new(struct TypeDef tdef, Span span) {
	struct Item it = { .tag = TypeDefItem };
	it.it.tdef = tdef;
	it.span = span;
	return it;
}

struct Item functiondefitem_new(struct FunctionDef fdef, Span span) {
	struct Item it = { .tag = FunctionDefItem };
	it.it.fdef = fdef;
	it.span = span;
	return it;
}

struct Item typeimplitem_new(struct TypeImpl timpl, Span span) {
	struct Item it = { .tag = TypeImplItem };
	it.it.timpl = timpl;
	it.span = span;
	return it;
}

struct Item vardefitem_new(struct VarDef vdef, Span span) {
	struct Item it = { .tag = VarDefItem };
	it.it.vdef = vdef;
	it.span = span;
	return it;
}

char const* item_to_string(struct Item const* this) {
	usize total_len = 0;
	char* res = (char*) malloc(sizeof(char));
	res[0] = '\0';
	strextend(&res, &total_len, "(");
	char const* field = NULL;
	switch (this->tag) {
		case TypeDefItem:
			strextend(&res, &total_len, "Item::TypeDefItem ");
			field = typedef_to_string(&this->it.tdef);
			strextend(&res, &total_len, field);
			strextend(&res, &total_len, " ");
			break;
		case FunctionDefItem:
			strextend(&res, &total_len, "Item::FunctionDefItem ");
			field = functiondef_to_string(&this->it.fdef);
			strextend(&res, &total_len, field);
			strextend(&res, &total_len, " ");
			break;
		case TypeImplItem:
			strextend(&res, &total_len, "Item::TypeImplItem ");
			field = typeimpl_to_string(&this->it.timpl);
			strextend(&res, &total_len, field);
			strextend(&res, &total_len, " ");
			break;
		case VarDefItem:
			strextend(&res, &total_len, "Item::VarDefItem ");
			field = vardef_to_string(&this->it.vdef);
			strextend(&res, &total_len, field);
			strextend(&res, &total_len, " ");
			break;
	}
	res[total_len - 1] = ')';
	res[total_len] = '\0';
	(void) field; // if enum has no variant

	return res;
}

struct Stmt itemstmt_new(struct Item i, Span span) {
	struct Stmt it = { .tag = ItemStmt };
	it.it.i = i;
	it.span = span;
	return it;
}

struct Stmt assignmentstmt_new(struct Assignment a, Span span) {
	struct Stmt it = { .tag = AssignmentStmt };
	it.it.a = a;
	it.span = span;
	return it;
}

struct Stmt exprstmt_new(struct Expr e, Span span) {
	struct Stmt it = { .tag = ExprStmt };
	it.it.e = e;
	it.span = span;
	return it;
}

char const* stmt_to_string(struct Stmt const* this) {
	usize total_len = 0;
	char* res = (char*) malloc(sizeof(char));
	res[0] = '\0';
	strextend(&res, &total_len, "(");
	char const* field = NULL;
	switch (this->tag) {
		case ItemStmt:
			strextend(&res, &total_len, "Stmt::ItemStmt ");
			field = item_to_string(&this->it.i);
			strextend(&res, &total_len, field);
			strextend(&res, &total_len, " ");
			break;
		case AssignmentStmt:
			strextend(&res, &total_len, "Stmt::AssignmentStmt ");
			field = assignment_to_string(&this->it.a);
			strextend(&res, &total_len, field);
			strextend(&res, &total_len, " ");
			break;
		case ExprStmt:
			strextend(&res, &total_len, "Stmt::ExprStmt ");
			field = expr_to_string(&this->it.e);
			strextend(&res, &total_len, field);
			strextend(&res, &total_len, " ");
			break;
	}
	res[total_len - 1] = ')';
	res[total_len] = '\0';
	(void) field; // if enum has no variant

	return res;
}

struct ItemSeq itemseq_new(struct ItemNode* head, Span span) {
	struct ItemSeq it = {
		.head = head,
		.span = span,
	};
	return it;
}

void itemseq_push(struct ItemSeq* seq, struct Item el) {
	struct Item* pel = (struct Item*) malloc(sizeof(struct Item));
	*pel = el;
	struct ItemNode new_node = itemnode_new(pel);
	new_node.next = seq->head;
	struct ItemNode* pnode = (struct ItemNode*) malloc(sizeof(struct ItemNode));
	*pnode = new_node;
	seq->head = pnode;
}

struct Item* itemseq_get(struct ItemSeq* seq, usize i) {
	return itemnode_get(seq->head, i);
}

char const* itemseq_to_string(struct ItemSeq const* this) {
	usize total_len = 0;
	char* res = (char*) malloc(sizeof(char));
	res[0] = '\0';

	for (ItemNode* curr = this->head; curr != NULL; curr = curr->next) {
		char const* el = item_to_string(curr->val);
		strextend(&res, &total_len, el);
		strextend(&res, &total_len, " ");
	}

	if (this->head) {
		res[total_len - 1] = ')';
		res[total_len] = '\0';
	}

	return res;
}

struct ItemNode itemnode_new(struct Item* val) {
	struct ItemNode it = {
		.val = val,
		.next = NULL,
	};
	return it;
}

struct Item* itemnode_get(struct ItemNode* node, usize i) {
	if (i == 0) {
		return node->val;
	} else {
		assertm(node->next != NULL, "index out of bounds!");
		return itemnode_get(node->next, i - 1);
	}
}

struct Program program_new(struct ItemSeq items, Span span) {
	struct Program it = {
		.items = items,
		.span = span,
	};
	return it;
}

char* program_to_string(struct Program const* this) {
	usize total_len = 0;
	char* res = (char*) malloc(sizeof(char));
	res[0] = '\0';
	strextend(&res, &total_len, "(Program ");

	if (&this->items) {
		char const* field = itemseq_to_string(&this->items);
		strextend(&res, &total_len, field);
		strextend(&res, &total_len, " ");
	}
	res[total_len - 1] = ')';
	res[total_len] = '\0';

	return res;
}
