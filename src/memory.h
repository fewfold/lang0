#pragma once

#ifndef MEMORY_H
#define MEMORY_H

#include "types.h"

#undef NULL
#define NULL 0

extern void* malloc(usize);
extern void free(void*);
extern void* realloc(void*, usize);

#endif /* MEMORY_H */
