override CC:=gcc

TARGET:=target/lang0
INSTALL_PATH:=$(HOME)/.local/bin/$(notdir $(TARGET))

LEXER:=src/lexer
PARSER:=src/parser
AST:=src/ast
SOURCES:=$(wildcard src/*.c) $(LEXER).c $(PARSER).c $(AST).c

OBJECTS:=$(patsubst src/%.c,target/%.o,$(SOURCES))

DEPENDS:=$(OBJECTS:.o=.d)

CFLAGS+=-Wall -Wextra -std=gnu18

LFLAGS+=-Wno-implicit-function-declaration\
		-Wno-unused-function

YFLAGS+=-Wno-implicit-function-declaration\
        -Wno-unused-function

LDFLAGS+=-B/usr/libexec/mold -lm

HLCOLOR:=\e[1;96m
RESET:=\e[0m

log_generating:="  $(HLCOLOR)generating$(RESET) "
 log_compiling:="   $(HLCOLOR)compiling$(RESET) "
   log_linking:="     $(HLCOLOR)linking$(RESET) "
   log_running:="     $(HLCOLOR)running$(RESET) "
log_installing:="  $(HLCOLOR)installing$(RESET) "
  log_removing:="    $(HLCOLOR)removing$(RESET) "
  log_cleaning:="    $(HLCOLOR)cleaning$(RESET) "
log_extracting:="  $(HLCOLOR)extracting$(RESET) "
log_formatting:="  $(HLCOLOR)formatting$(RESET) "

-include $(DEPENDS)

.PHONY: all fmt build run clean install uninstall

all: build fmt

$(LEXER).c: $(LEXER).l $(PARSER).h Makefile
	@printf $(log_generating)
	flex --header-file=$(@:.c=.h) -o $@ $<

$(LEXER).h: $(LEXER).c Makefile

$(PARSER).c: $(PARSER).y Makefile
	@printf $(log_generating)
	bison --header=$(@:.c=.h) --report-file=$(@:.c=.output) -o $@ $<

$(PARSER).h: $(PARSER).c Makefile

$(AST).c: tools/generate_ast.py $(AST).gen Makefile
	@printf $(log_generating)
	./$<

$(AST).h: $(AST).c

$(OBJECTS): | $(LEXER).c $(PARSER).c $(AST).c

target/%.o: src/%.c Makefile
	@printf $(log_compiling)
	@mkdir -p target
	$(CC) -MMD $(CFLAGS)$(if $(filter $(notdir $(LEXER)),$*), $(LFLAGS))$(if $(filter $(notdir $(PARSER)),$*), $(YFLAGS)) $< -c -o $@

$(TARGET): $(OBJECTS)
	@printf $(log_linking)
	$(CC) $(LDFLAGS) $^ -o $@

$(INSTALL_PATH): build
	@printf $(log_installing)
	cp $(TARGET) $@

fmt: .clang-format 
	@printf $(log_formatting)
	clang-format --verbose -i src/*.c src/*.h

build: $(TARGET)

run: build
	@printf $(log_running)
	./$(TARGET)

install: $(INSTALL_PATH)

uninstall:
	@printf $(log_removing)
	$(RM) $(HOME)/.local/bin/$(notdir $(TARGET))

clean:
	@printf $(log_cleaning)
	$(RM) -r target $(LEXER).c $(LEXER).h $(PARSER).c $(PARSER).h $(PARSER).output sample

ifneq ($(shell which rg),)
sample: LANG0.md
	@printf $(log_extracting)
	rg -N --color never -U --only-matching '```lang0(\p{any}*?)```' $< -r 'fn snip() begin $$1 end' > $@
else
sample:
	$(error install ripgrep to use this recipe)
endif
