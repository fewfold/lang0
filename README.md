# lang0

lang0 compiler in C using flex, bison and some python.

# Build & Run

To build the compiler and compile every code sample in [LANG0.md](LANG0.md):
```
make build && ./target/lang0 -l fib.l0
```
You can also install the compiler binary using `make install` (and remove it
using `make uninstall`).
