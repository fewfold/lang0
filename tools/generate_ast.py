#!/usr/bin/env python3

from dataclasses import dataclass, field
from typing import Any, Tuple
import re

GENERATED_TYPES_LIST : dict[str, str] = {}

@dataclass
class Node():
    def __str__(self) -> str:
        pass

    def gen_header(self)-> str:
        pass

    def gen_source(self)-> str:
        pass

    def gen_type_def(self) -> str:
        pass

TYPES : list[Node] = list()

@dataclass
class KnownType(Node):
    name: str

    def __str__(self) -> str:
        if self.name in GENERATED_TYPES_LIST:
            return GENERATED_TYPES_LIST[self.name]
        else:
            return self.name

    def gen_header(self)-> str:
        return ""

    def gen_source(self)-> str:
        return ""

    def gen_type_def(self) -> str:
        return ""

@dataclass
class Struct(Node):
    name: str
    fields: dict[str, Node]
    default_values: dict[str, Any] = field(default_factory=dict)
    span : None | Node = KnownType("Span")
    function_decls : list[str] = field(default_factory=list)
    function_defs : list[str] = field(default_factory=list)

    def __post_init__(self):
        GENERATED_TYPES_LIST[self.name] = str(self)
        if self.span is not None:
            self.fields["span"] = self.span

        TYPES.append(self)

    def __str__(self) -> str:
        return f"struct {self.name}"

    def gen_type_def(self) -> str:
        gen = f"{self} {{\n"

        for name, ty in self.fields.items():
            gen += f"\t{ty} {name};\n"

        gen += "};\n"

        gen += f"typedef {self} {self.name};\n"

        return gen

    def gen_constructor_decl(self) -> str:
        gen = f"{self} {self.name.lower()}_new("

        for field, type in self.fields.items():
            if field not in self.default_values:
                gen += f"{type} {field}, "

        gen = gen.rstrip(", ")

        gen += ")"
        return gen

    def gen_constructor_def(self) -> str:
        gen = self.gen_constructor_decl() + " {\n"

        gen += f"\t{self} it = {{\n";

        for field, type in self.fields.items():
            if field in self.default_values:
                gen += f"\t\t.{field} = {self.default_values[field]},\n"
            else:
                gen += f"\t\t.{field} = {field},\n"
        gen += "\t};\n"

        gen += "\treturn it;\n}\n"
        return gen

    def gen_to_string_def(self) -> str:
        if self.name.endswith("Seq"):
            return self.gen_seq_to_string_def()
        elif self.name.endswith("Node"):
            return ""
        else:
            return self.gen_not_seq_to_string_def()

    def gen_to_string_decl(self) -> str:
        if self.name.endswith("Seq"):
            return self.gen_seq_to_string_decl()
        elif self.name.endswith("Node"):
            return ""
        else:
            return self.gen_not_seq_to_string_decl()

    def gen_seq_to_string_decl(self):
        return f"char const* {self.name.lower()}_to_string({self} const* this)"

    def gen_seq_to_string_def(self):
        inner = self.name.removesuffix("Seq")
        gen = self.gen_seq_to_string_decl() + "{\n"
        gen += f"""\
    usize total_len = 0; char* res = (char*) malloc(sizeof(char)); res[0] = '\\0';

    for ({inner}Node* curr = this->head; curr != NULL; curr = curr->next) {{
        char const* el = {inner.lower()}_to_string(curr->val);
        strextend(&res, &total_len, el);
        strextend(&res, &total_len, " ");
    }}

    if (this->head) {{
        res[total_len-1] = ')';
        res[total_len] = '\\0';
    }}

    return res;
"""            
        return gen + "}\n"

    def gen_not_seq_to_string_decl(self) -> str:
        return f"char* {self.name.lower()}_to_string({self} const* this)"

    def gen_not_seq_to_string_def(self) -> str:
        gen = self.gen_to_string_decl() + "{\n";
        gen += f"""\
    usize total_len = 0; char* res = (char*) malloc(sizeof(char)); res[0] = '\\0';
    strextend(&res, &total_len, "({self.name} ");
"""
        for field, type  in self.fields.items():
            if field == "span":
                continue;
            this_field = f"&this->{field}"
            if isinstance(type, Ptr):
                type = type.inner
                this_field = f"this->{field}"
            elif isinstance(type, Seq):
                type = type.seq
            gen += f"""\n\
    if ({this_field}) {{
        char const* field = {type.name.lower()}_to_string({this_field});
        strextend(&res, &total_len, field);
        strextend(&res, &total_len, " ");
    }}
"""

        gen += """\
    res[total_len-1] = ')';
    res[total_len] = '\\0';

    return res;
"""

        return gen + "}\n"

    def gen_header(self) -> str:
        gen = self.gen_type_def() + '\n'
        gen += self.gen_constructor_decl() + ";\n"
        
        for d in self.function_decls:
            gen += d + '\n'

        gen += self.gen_to_string_decl() + ";\n"

        return gen

    def gen_source(self)-> str:
        gen = self.gen_constructor_def() + '\n'

        for d in self.function_defs:
            gen += d + '\n'

        gen += self.gen_to_string_def() + '\n'

        return gen 

@dataclass
class Enum(Node):
    name: str
    variants: dict[str, None | dict[str, Node]]

    def __post_init__(self):
        GENERATED_TYPES_LIST[self.name] = str(self)
        TYPES.append(self)

    def __str__(self) -> str:
        return f"struct {self.name}"

    def gen_type_def(self) -> str:
        gen = f"{self} {{\n"
        gen += "\tSpan span;\n"

        gen += "\tenum {\n"
        has_data = False
        for name, data in self.variants.items():
            if data is not None:
                has_data = True
            gen += f"\t\t{name},\n"
        gen += "\t} tag;\n"

        if has_data:
            gen += "\tunion {\n"
            all_fields: dict[str, str] = {}
            for name, data in self.variants.items():
                if data is None:
                    continue

                gen_fields = ""
                for field, type in data.items():
                    if field not in all_fields:
                       gen_fields += f"\t\t\t{type} {field};\n" 
                       all_fields[field] = str(type)
                    elif field in all_fields and all_fields[field] != str(type):
                        assert False, f"field `{field}` duplicated in `{self.name}`"

                if len(gen_fields) > 0:
                    gen += "\t\tstruct {\n"
                    gen += gen_fields
                    gen += "\t\t};\n"

            gen += "\t} it;\n"

        gen += "};\n"

        gen += f"typedef {self} {self.name};\n"

        return gen

    def gen_constructors_decl(self) -> str:
        gen = ""

        for variant, data in self.variants.items():
            gen += f"{self} {variant.lower()}_new("
            
            if data is None:
                data = {}

            for field, type in data.items():
                gen += f"{type} {field}, "

            gen += "Span span"
            gen += ");\n"

        return gen

    def gen_constructors_def(self) -> str:
        gen = ""
        for variant, data in self.variants.items():
            gen += f"{self} {variant.lower()}_new("
            
            if data is None:
                data = {}

            for field, type in data.items():
                gen += f"{type} {field}, "

            gen += "Span span"
            gen += ") {\n"

            gen += f"\t{self} it = {{ .tag={variant} }};\n"

            for field, type in data.items():
                gen += f"\tit.it.{field}={field};\n"
            gen += "\tit.span=span;\n"
            
            gen += "\treturn it;\n"
            gen += "}\n\n"

        return gen


    def gen_to_string_decl(self) -> str:
        return f"char const* {self.name.lower()}_to_string({self} const* this)"

    def gen_to_string_def(self) -> str:
        gen = self.gen_to_string_decl() + "{\n"
        gen += f"""\
    usize total_len = 0; char* res = (char*) malloc(sizeof(char)); res[0] = '\\0';
    strextend(&res, &total_len, "(");
    char const* field = NULL;
"""
        gen += "\tswitch (this->tag) {\n";

        for variant, data in self.variants.items():
            gen += f"\t\tcase {variant}: \n"
            
            gen += f"\t\t\tstrextend(&res, &total_len, \"{self.name}::{variant} \");\n"
            if data is not None:
                for field, type in data.items():
                    this_field = f"&this->it.{field}"
                    if isinstance(type, Ptr):
                        type = type.inner
                        this_field = f"this->it.{field}"
                    elif isinstance(type, Seq):
                        type = type.seq
                    gen += f"""\
            field = {type.name.lower()}_to_string({this_field});
            strextend(&res, &total_len, field);
            strextend(&res, &total_len, " ");
"""
            gen += "\t\t\tbreak;\n"
        gen += "\t}\n"


        gen += """\
    res[total_len-1] = ')';
    res[total_len] = '\\0'; 
    (void) field; // if enum has no variant

    return res;
"""

        return gen + "}\n"

    def gen_header(self)-> str:
        gen = self.gen_type_def() + '\n'
        gen += self.gen_constructors_decl() + '\n'
        gen += self.gen_to_string_decl() + ";\n"

        return gen

    def gen_source(self)-> str:
        gen = self.gen_constructors_def()
        gen += self.gen_to_string_def() + '\n'

        return gen 

@dataclass
class Ptr(Node):
    inner: Node

    def __post_init__(self):
        pass

    def __str__(self) -> str:
        return f"{self.inner}*"

    def gen_header(self)-> str:
        return self.inner.gen_header()

    def gen_source(self)-> str:
        return self.inner.gen_source()

    def gen_type_def(self) -> str:
        return self.inner.gen_type_def()

@dataclass
class Seq(Node):
    inner: Struct | Enum | KnownType
    node: None | Node = None
    seq: None | Node = None
    node_name: None | str = None
    seq_name: None | str = None

    def __post_init__(self):
        self.node_name = f"{self.inner.name}Node"
        self.seq_name = f"{self.inner.name}Seq"
        if self.seq_name in GENERATED_TYPES_LIST:
            self.node = KnownType(self.node_name)
            self.seq = KnownType(self.seq_name)
        else:
            self.seq = Struct(
                    self.seq_name,
                    {
                        "head": Ptr(KnownType(self.node_name)),
                    }
            )

            self.node = Struct(
                    f"{self.inner.name}Node",
                    {
                        "val": Ptr(KnownType(f"{self.inner.name}")),
                        "next": Ptr(KnownType(self.node_name))
                    },
                    default_values = {
                        "next" : "NULL"
                    },
                    span = None,
                )

            self.seq.function_decls.append(self.gen_push_decl() + ";")
            self.seq.function_decls.append(self.gen_seq_get_decl() + ";")
            self.seq.function_defs.append(self.gen_push_def())
            self.seq.function_defs.append(self.gen_seq_get_def())
            self.node.function_decls.append(self.gen_node_get_decl() + ";")
            self.node.function_defs.append(self.gen_node_get_def())


            GENERATED_TYPES_LIST[self.seq.name] = str(self.seq)

    def __str__(self) -> str:
        return str(self.seq)

    def gen_push_decl(self) -> str:
        gen = f"void {self.seq_name.lower()}_push"
        el_type = ""
        if self.inner.name not in GENERATED_TYPES_LIST:
            el_type = KnownType(f"struct {self.inner.name}")
        else:
            el_type = self.inner

        gen += f"({Ptr(KnownType(self.seq_name))} seq, {el_type} el)"
        return gen

    def gen_push_def(self) -> str:
        gen = self.gen_push_decl()

        el_type = ""
        if self.inner.name not in GENERATED_TYPES_LIST:
            el_type = KnownType(f"struct {self.inner.name}")
        else:
            el_type = self.inner

        gen += f"""{{
    {Ptr(el_type)} pel = ({Ptr(el_type)}) malloc(sizeof({el_type}));
    *pel = el;
    {self.node} new_node = {self.node.name.lower()}_new(pel);
    new_node.next = seq->head;
    {Ptr(self.node)} pnode = ({Ptr(self.node)}) malloc(sizeof({self.node}));
    *pnode = new_node;
    seq->head=pnode;
}}
"""
        return gen
    
    def gen_seq_get_decl(self) -> str:
        el_type = ""
        if self.inner.name not in GENERATED_TYPES_LIST:
            el_type = KnownType(f"struct {self.inner.name}")
        else:
            el_type = self.inner
        gen = f"{Ptr(el_type)} {self.seq_name.lower()}_get"
        gen += f"({Ptr(KnownType(self.seq_name))} seq, usize i)"
        return gen

    def gen_seq_get_def(self) -> str:
        gen = self.gen_seq_get_decl()
        gen += f"""{{
    return {self.node_name.lower()}_get(seq->head, i);
}}
"""
        return gen

    def gen_node_get_decl(self) -> str:
        el_type = ""
        if self.inner.name not in GENERATED_TYPES_LIST:
            el_type = KnownType(f"struct {self.inner.name}")
        else:
            el_type = self.inner
        gen = f"{Ptr(el_type)} {self.node_name.lower()}_get"
        gen += f"({Ptr(KnownType(self.node_name))} node, usize i)"
        return gen

    def gen_node_get_def(self) -> str:
        gen = self.gen_node_get_decl()
        gen += f"""{{
    if (i == 0) {{
        return node->val;
    }} else {{
        assertm(node->next != NULL, "index out of bounds!");
        return {self.node_name.lower()}_get(node->next, i-1);
    }}
}}
"""
        return gen

@dataclass
class CodeGen:
    nodes: list[Node]

    def gen_header(self) -> str:
        gen:str ="""\
/********************************************************************
 * This file was generated with the `tools/generate_ast.py` script. *
 * DO NOT MODIFY!                                                   *
 ********************************************************************/

#pragma once
#ifndef AST_H
#define AST_H

#include "prelude.h"
#include "diagnostics.h"

"""

        for _, ty in GENERATED_TYPES_LIST.items():
            gen += f"{ty};\n"

        for node in self.nodes:
            gen += node.gen_header()

        gen += "\n#endif /* AST_H */"

        return gen

    def gen_source(self) -> str:
        gen:str ="""\
/********************************************************************
 * This file was generated with the `tools/generate_ast.py` script. *
 * DO NOT MODIFY!                                                   *
 ********************************************************************/

#include "ast.h"

extern usize strlen(char const*);
extern char* strncat(char*, char const*, usize);

static inline void strextend(char** dest, usize* dlen, char const* src) {
    usize slen = strlen(src);
    *dlen += slen;
    *dest = (char*) realloc(*dest, (*dlen + 1 < 32 ? 32 : *dlen + 1)*sizeof(char));
    *dest = strncat(*dest, src, slen);
}

"""

        for node in self.nodes:
            gen += node.gen_source()

        return gen

@dataclass
class CodeGenParser:
    src: str
    tokens: list[str] = field(default_factory=list)
    curr: int = 0

    def __post_init__(self):
        self.tokens = [
                t for t in re.split(r"\s+|([():{},])", self.src) 
                  if t is not None and len(t) > 0
            ]

    def peek(self) -> str:
        return self.tokens[self.curr]

    def next(self) -> str:
        self.curr += 1
        return self.tokens[self.curr - 1]

    def next_ident(self) -> str:
        assert self.peek() not in [',', ':', '{', '}', '(', ')']
        return self.next()

    def next_opened_brace(self) -> str:
        assert self.peek() == '{', f"expected '{{', found {self.peek()} at {self.curr}"
        return self.next()

    def next_closed_brace(self) -> str:
        assert self.peek() == '}', f"expected '}}', found {self.peek()} at {self.curr}"
        return self.next()

    def next_opened_paren(self) -> str:
        assert self.peek() == '(', f"expected '(', found {self.peek()} at {self.curr}"
        return self.next()

    def next_closed_paren(self) -> str:
        assert self.peek() == ')', f"expected ')', found {self.peek()} at {self.curr}"
        return self.next()

    def next_comma(self) -> str:
        assert self.peek() == ',', f"expected ',', found {self.peek()} at {self.curr}"
        return self.next()

    def next_colon(self) -> str:
        assert self.peek() == ':', f"expected ':', found {self.peek()} at {self.curr}"
        return self.next()

    def parse_known(self) -> KnownType:
        ty = ""
        while self.peek() not in [',', ')', '}']:
            ty += self.next()
            ty += ' '
        
        return KnownType(ty.strip())

    def parse_typed_param(self) -> Tuple[str, Node]:
        name = self.next_ident()
        self.next_colon()
        type = self.parse_node()
        return name, type

    def parse_typed_param_list(self) -> dict[str, Node]:
        params = {}
        while self.peek() not in ['}', ')']:
            name, type = self.parse_typed_param()
            params[name] = type
            if self.peek() == ',':
                self.next()
        return params

    def parse_enum_variants(self) -> Tuple[str, None | dict[str, Node]]:
        name = self.next_ident()
        fields : None | dict[str, Node]
        if self.peek() == '(':
            self.next_opened_paren()
            fields = self.parse_typed_param_list()
            self.next_closed_paren()
        else:
            fields = None
        self.next_comma()
        return name, fields

    def parse_enum(self) -> Enum:
        self.next() # enum token
        name = self.next_ident()
        self.next_opened_brace()
        variants = {}
        while self.peek() != '}':
            variant, fields = self.parse_enum_variants()
            variants[variant] = fields
        self.next_closed_brace()
        return Enum(name, variants)

    def parse_struct(self) -> Struct:
        self.next() # struct token
        name = self.next_ident()
        self.next_opened_brace()
        fields = self.parse_typed_param_list()
        self.next_closed_brace()
        return Struct(name, fields)

    def parse_node(self) -> Node:
        node : None | Node = None
        ptr = False
        seq = False

        while node is None:
            match self.peek():
                case 'struct':
                    node = self.parse_struct()
                case 'enum':
                    node = self.parse_enum()
                case 'ptr':
                    ptr = True
                    self.next()
                case 'seq':
                    seq = True
                    self.next()
                case '{' | '}' | ',' | ';' | ':':
                    assert False, f"unexpected punctuation at {self.curr}"
                case _:
                    node = self.parse_known()

        if ptr:
            node = Ptr(node)
        if seq:
            node = Seq(node)

        return node

    def parse(self) -> CodeGen:
        while self.curr < len(self.tokens):
            self.parse_node()

        return CodeGen(TYPES)

def main():
    file: str = "src/ast"

    with (
        open(file + ".gen", 'r', encoding="utf-8") as d,
        open(file + ".h", 'w', encoding="utf-8") as h,
        open(file + ".c", 'w', encoding="utf-8") as c
    ):
        gen_parser = CodeGenParser(d.read())
    

        gen = gen_parser.parse()

        h.write(gen.gen_header())
        c.write(gen.gen_source())

if __name__ == "__main__":
    main()
